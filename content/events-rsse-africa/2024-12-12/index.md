---
# Homepage
type: widget_page

title: Open Research Software

# Homepage is headless, other widget pages are not.
headless: false
---


  {{% callout note %}}
    Watch this page for updates on our speaker panels!
  {{% /callout %}}