---
widget: blank
headless: true  # This file represents a page section.
title: "Meetup Resources"
subtitle: 

weight: 30   

design:
    columns: "1"
---

<br>

<div class="container">
    <div class="row">
        <div class="col-6">
            <p align="center">
                View our <a href="https://youtu.be/y0z324Jwn4Y?si=LrEpY7jLGng_BMzD" target="_blank">Recording</a>
                <br>
                <iframe width="560" height="315" src="https://www.youtube.com/embed/y0z324Jwn4Y?si=hahbjkJiwfZjqeiM" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>
            </p>
        </div>
        <div class="col-3">
            <p align="center">
                Access our <a href="https://doi.org/10.5281/zenodo.14195613" target="_blank">Resources Sheet</a>
                <br>
                <a href="https://doi.org/10.5281/zenodo.14195613" target="_blank"><img src="resource-sheet.png" width="70%"></a>
            </p>
        </div>
        <div class="col-3">
            <p align="center">
                Read our <a href="../../post/2024/12/open-science-episode2/">summary blog post</a>
                <br>
                <a href="../../post/2024/12/open-science-episode2/"><img src="blog-post.png" width="70%"></a>
            </p>
        </div>
    </div>
</div>