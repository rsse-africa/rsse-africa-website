---
widget: blank
headless: true  # This file represents a page section.
title: "Episode 2: Enabling Reproducibility through Research Code"
subtitle: |
    <br>

    <h3>14 November 2024 @ 8:30 - 10:00 am UTC <a href="https://www.timeanddate.com/worldclock/fixedtime.html?msg=Reproducibility+and+Research+Software&iso=20241114T0830&p1=1440&ah=1&am=30">(your local time)</a></h3>

    <br>

    Do you spend a lot of time using programming to analyse your data, develop workflows, build models or create visualisations?

    This episode focused on steps to take, to ensure your code enables reproducibility in your research project. We explored a variety of topics ranging from easily implementable, small changes that can make a huge difference (such as having a good directory structure), to more complex practices like the use of containers to manage versions and dependencies.

design:
    columns: "1"

weight: 10
---


{{% callout info %}}
  A useful open educational resource related to these topics are available from [CodeRefinery](https://coderefinery.org/).

  __[Follow the CodeRefinery lesson on Reproducible research](https://coderefinery.github.io/reproducible-research/)__

{{% /callout %}}

