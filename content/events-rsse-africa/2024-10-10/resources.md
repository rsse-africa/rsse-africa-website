---
widget: blank
headless: true  # This file represents a page section.
title: "Meetup Resources"
subtitle: 

weight: 30   

design:
    columns: "1"
---

<br>

<div class="container">
    <div class="row">
        <div class="col-6">
            <p align="center">
                View our <a href="https://youtu.be/UmSHhDVRGig?si=DGRFxBJU1Hm5f6aT" target="_blank">Recording</a>
                <br>
                <iframe width="560" height="315" src="https://www.youtube.com/embed/UmSHhDVRGig?si=Upp6ru-EN3BdMJsD" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" referrerpolicy="strict-origin-when-cross-origin" allowfullscreen></iframe>
            </p>
        </div>
        <div class="col-3">
            <p align="center">
                Access our <a href="https://doi.org/10.5281/zenodo.13946139" target="_blank">Resources Sheet</a>
                <br>
                <a href="https://doi.org/10.5281/zenodo.13946139" target="_blank"><img src="resource-sheet.png" width="70%"></a>
            </p>
        </div>
        <div class="col-3">
            <p align="center">
                Read our <a href="../../post/2024/10/open-science-episode1/" target="_blank">Blog Post</a>
                <br>
                <a href="../../post/2024/10/open-science-episode1/" target="_blank"><img src="blog-post.png" width="70%"></a>
            </p>
        </div>
    </div>
</div>