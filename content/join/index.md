---
# Leave the homepage title empty to use the site title
title: 
date: 2023-01-27
type: landing
sections:
  - block: features
    content:
      title: Want to join us? <br><br>
      subtitle: Choose the platform that works best for you. We look forward to seeing you in the community! <br><br>
      items:
        - name: <a href="../events/" target=_blank>Join our meetups</a>
          description: We'll resume our monthly community meetups on 10 October 2024. Please join us! <br> <a href="">Learn more</a>.
          icon: users
          icon_pack: fas
        - name: <a href="https://www.linkedin.com/groups/12903402/" target=_blank>Connect on LinkedIn</a>
          description: Ask questions, find out about opportunities and share resources, and much more via our newly formed LinkedIn Group. 
          icon: comments
          icon_pack: fas
        - name: <a href="https://ukrse.slack.com/join/signup" target=_blank>Join our Slack channel</a>
          description: "Connect with peers from across the world in the International RSE Slack workspace where we also host a dedicated _#rsse-africa_ channel." 
          icon: comments
          icon_pack: far
        - name: <a href="../project/mapping" target=_blank>Get mapped</a>
          description: We're trying to find all research software developers, funders, and other stakeholders on the continent and adding them to a database and interactive map. Add yourself to the list or share with others.
          icon: map-marker-alt
          icon_pack: fas
        - name: <a href="https://talarify.us14.list-manage.com/subscribe?u=35d5db26d3b108b9ef9b9ac43&id=55e9f5a692" target=_blank>Read our Newsletter</a>
          description: We're trying to find all research software developers, funders, and other stakeholders on the continent and adding them to a database and interactive map. Add yourself to the list or share with others.
          icon: map-marker-alt
          icon_pack: fas
        - name: <a href="../project/spotlight" target=_blank>Be in the spotlight</a>
          description: We're trying to find all research software developers, funders, and other stakeholders on the continent and adding them to a database and interactive map. Add yourself to the list or share with others.
          icon: lightbulb
          icon_pack: fas 
---