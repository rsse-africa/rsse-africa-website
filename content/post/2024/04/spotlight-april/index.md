---
title: "🇨🇩 April 2024: Meet Richard Dushime"

subtitle: 

# Summary for listings and search engines
summary:  Welcome 👋 to our April 2024 community member spotlight! In this month's edition we're introducing Richard Dushime from The Democratic Republic of the Congo.

# Link this post with a project
projects: []

# Date published
date: '2024-04-27T00:00:00Z'


# Date updated
lastmod: '2024-04-27T00:00:00Z'

# Is this an unpublished draft?
draft: false

# Show this page in the Featured widget?
featured: false

# Featured image
# Place an image named `featured.jpg/png` in this page's folder and customize its options here.
image:
  caption: 'Image credit: Richard Dushime'
  focal_point: ''
  placement: 2
  preview_only: false

authors:
  - Richard Dushime

tags:
  - Community members
  - Open Source
  - RSEAA24
  - The Democratic Republic of the Congo
  

categories:
  - Spotlights
  - The Democratic Republic of the Congo
---

Our April spotlight shines on Richard Dushime from The Democratic Republic of the Congo. Richard is the  Secretary at RSEAA24, the RSE Asia Australia unconference, and  an open-source contributor.

📣 **Exciting News** 

We're thrilled to present our first bilingual spotlight. For this edition, Richard graciously shared his insights in both English and [French](../../../2024/04/spotlight-april-french/), enhancing accessibility and engagement for our French-speaking community members.

## In short
---

- __Preferred name and surname__: Richard Dushime
- __Affiliation__ (where do you work or study): Research Software Engineer Asia Australia Organising Committee (RSEAA24) on behalf of RSE Australia New Zealand and RSE Asia
- __Role__:  Secretary
- __Email__: mudaherarich@gmail.com 
- __LinkedIn__: https://www.linkedin.com/in/richard-dushime/
- __Twitter(X)__: https://twitter.com/RichardDushime 
- __Website__: https://richarddushime.netlify.app/ 

---


{{< spoiler text="__:musical_score: What is your favourite song?__" >}}

Well, I may not have a specific one but generally slow music is my favorite, and it depends on my mood anyway.

{{< /spoiler >}}

{{< spoiler text="__:books: Can you describe your background and your current role?__" >}}

#### What did you study?

I pursued a Bachelor's degree in Business Information Systems at Bugema University in Kampala, Uganda, and I am currently seeking admission for a Master's program in Data Science or Computer science-related course.

#### What is the title of your current role?

Currently, I hold the position of Secretary at RSEAA24, the RSE Asia Australia unconference, and am an open-source contributor.

#### Give a one-sentence summary of what you do in this role

In my role as Secretary at RSEAA24, I coordinate and participate in weekly meetings, maintain the website, execute delegated tasks, update documentation, and ensure continuous improvement and adherence to Diversity, Equity, and Inclusion standards. Additionally, as an open-source contributor, I contribute to various projects like The Turing Way, Forrt, Student-intern-organiser at WEHI, and more.

#### How did you get involved with RSE communities?

My journey into Research Software Engineering (RSE) began through my involvement in open-source communities. It was through these communities that I met mentors like Elisee Jafsia, who introduced me to initiatives like Open Life Science Mentorship Training. Collaborating with individuals like Rowland Mosbergen on open-source projects further fueled my interest. Eventually, I was introduced to RSE communities and events, such as the RSEAA23 Unconference. Actively participating in the RSEAA23 organizing committee, allowed me to connect with RSE’s and explore opportunities within the field.

#### What inspired you to get involved in community building?

My inspiration to get involved in community building stemmed from personal challenges I faced during my early university years. Coming from a French-speaking background, I initially struggled to adapt to school activities conducted in English. However, joining a club in my second year provided a supportive network and helped me explore opportunities beyond the university. Recognizing the benefits of community support, I delved deeper into understanding and building communities. Eventually, I became the lead for Google Developer Student Club( GDSC) , which opened doors to various global communities. My inspiration lies in the mutual assistance and benefits fostered within communities.

#### What have you learned from contributing to Open Source projects?

Contributing to open-source projects has been a great learning experience. It's taught me about sharing knowledge and supporting others. I've also learned to work in a dynamic environment where things can change quickly. It's a continuous learning process that has enriched my skills and broadened my perspectives.

{{< /spoiler >}}

{{< spoiler text="__:briefcase: Describe a typical workday... Expand specifically on the amount of time you spend coding__" >}}

#### How much time do you spend coding?

I focus primarily on open-source software development, which involves coding most of the time.

#### How many projects do you work on?

Currently, I contribute to various projects, including The Turing Way, Forrt, Servo, RSEAA24, and many more

#### Give some keywords that summarise the topics of your projects.

Infrastructure, GitOps, DevOps, Code, Documentation.

#### How often do you work with non-coding researchers?

Not often


{{< /spoiler >}}

{{< spoiler text="__:school: Tell us more about the organisation where you work?__" >}}

#### What is their primary objective?

The primary objective is to maintain an active, diverse, and sustainable RSE community while defining the role of RSEs within the eResearch, Academic, and Professional context, and forming connections between individuals and organizations.


#### How many people in your organisation are involved in research software development (a thumbsuck is okay)?

The RSE Association of Australia and New Zealand (RSE-AUNZ) currently has 462 members.
The RSE Asia Association has recently been established and has 85 members.


{{< /spoiler >}}

{{< spoiler text="__:bulb: Where do you get training and support?__" >}}

#### Which communities of practice are you part of?

Most of my training and support comes from active involvement in different open-source communities and various student communities and clubs at universities.

#### What training has had an impact on your current career?

Participating in the OLS (Open Life Science) mentorship program has significantly contributed to my career growth.
Also participating in a practical diversity and inclusion fellowship at PDI


{{< /spoiler >}}

{{< spoiler text="__:thought_balloon: Do you see yourself as an academic, researcher, software engineer, technician…? All of it? Something else? A mix of one or two terms?__ " >}}

While I currently focus on software development through open-source contributions, I see myself transitioning into the role of a Research Software Engineer (RSE) in the future. This allows me to leverage coding while also supporting the research community.


{{< /spoiler >}}

{{< spoiler text="__:no_entry: What kind of barriers do you face in your work?__ " >}}

Finding paid opportunities to enhance my skill set or my RSE experience

{{< /spoiler >}}

{{< spoiler text="__:heart_eyes: What part of your job do you enjoy the most?__" >}}

I find great satisfaction in helping others when they encounter challenges and seeing growth and progress within open-source communities.

{{< /spoiler >}}

{{< spoiler text="__:sos: What would make things easier for you and support you in your work?__" >}}

Having clearer plans and guidelines for the tasks would help me.

{{< /spoiler >}}

{{< spoiler text="__:telescope: What are you looking forward to this year?__" >}}

I am looking forward to opportunities to pursue a Master's degree in Data Science or a related field, organizing the RSEAA24 event in September, and collaborating and participating in various workshops and events.


{{< /spoiler >}}

{{< spoiler text="__:speech_balloon: Please share the most helpful career advice you’ve received that you want to share with other Africans in similar roles.__" >}}

Learning never ends, and embracing change is essential for personal and professional growth. Additionally, helping others improves your knowledge and contributes to collective advancement.

{{< /spoiler >}}




