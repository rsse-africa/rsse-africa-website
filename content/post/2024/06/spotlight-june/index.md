---
title: "🇿🇦 June 2024: Meet Mthetho Vuyo Sovara"

subtitle: 

# Summary for listings and search engines
summary:  Welcome 👋 to our June 2024 community member spotlight! In this month's edition we're introducing Mthetho Vuyo Sovara from South Africa.

# Link this post with a project
projects: []

# Date published
date: '2024-06-18T00:00:00Z'


# Date updated
lastmod: '2024-06-18T00:00:00Z'

# Is this an unpublished draft?
draft: false

# Show this page in the Featured widget?
featured: false

# Featured image
# Place an image named `featured.jpg/png` in this page's folder and customize its options here.
image:
  caption: 'Image credit: Mthetho Vuyo Sovara'
  focal_point: ''
  placement: 2
  preview_only: false

authors:
  - Mthetho Vuyo Sovara

tags:
  - Community members
  - Ocean and Climate Dynamics
  - South Africa

categories:
  - Spotlights
  - South Africa
---

Our June spotlight shines on Mthetho Vuyo Sovara from South Africa. Mthetho is a Research Support Scientist at the Centre for High-Performance Computing (CHPC), part of the Council for Scientific and Industrial Research (CSIR). He is also a PhD candidate at the University of Cape Town.

📣 **Exciting News** 

This community spotlight is also available in [French](../../../2024/06/spotlight-june-french/).   

## In short
---

- __Preferred name and surname__: Mthetho Vuyo Sovara
- __Affiliation__ (where do you work or study):  Centre for High-Performance Computing (CHPC) at the Council for Scientific and Industrial Research (CSIR)
- __Role__:  Research Support Scientist 
- __Email__: msovara@csir.co.za
- __LinkedIn__: https://www.linkedin.com/in/mthetho-sovara-3a939272/
- __Twitter(X)__: https://x.com/MSovara
- __ORCID ID__: [0000-0002-0498-0179](https://orcid.org/0000-0002-0498-0179)

---


{{< spoiler text="__:musical_score: What is your favourite song?__" >}}

:sound: [Be Still & Know](https://www.youtube.com/watch?v=Ia7s2OE4PC4) – _Housefires feat. DOE_ 


{{< /spoiler >}}

{{< spoiler text="__:books: Can you describe your background and your current role?__" >}}

#### What did you study?

My undergrad studies in Environmental Sciences (BSc with Honours) were completed at Walter Sisulu University (WSU), Mthatha campus. 


#### Do you have postgrad qualifications? In what area?

I currently hold an MSc (Ocean and Climate Dynamics) from the University of Cape Town, where I am also pursuing my PhD.

#### What is the title of your current role?

I am currently occupying the position of research support scientist for the domains of Earth System Science and Astrophysics at the Centre for High-Performance Computing (CHPC).

#### Give a one-sentence summary of what you do in this role

I support scientists in utilizing the CHPC's computing resources. 

{{< /spoiler >}}

{{< spoiler text="__:briefcase: Describe a typical workday... Expand specifically on the amount of time you spend coding__" >}}

#### How much time do you spend coding?

I spend a significant part (>60%) of my day, every day, examining user codes and debugging their high-performance computing (HPC) issues. This typically involves sanitizing HPC environments, building and installing simple to complex software, resolving runtime errors, and helping to optimize user workflows. Additionally, I study various weather and climate application models to better understand their system requirements and workflows, as I am also involved in various research projects.

#### How many projects do you work on?

At the CHPC, I support approximately 40 research programmes within the Earth Systems Science and Astrophysics domains. I am also actively involved with various CHPC Human Capital Development activities such as the :
1. Student Cluster Competition,
2. The SADC HPC Ecosystems Project,
3. CHPC Science Engagement (NICIS strategic leadership), 
4. CHPC Summer and Winter Schools, 
5. The National Department of Science and Innovation’s Coding, Programming and Robotics project. 
6. SADC Cyber Infrastructure activities and regional climate change.
7. CHPC Schools Outreach Programme.  


#### Give some keywords that summarise the topics of your projects.

HPC, Hardware, Networking, Software, Parallel Computing, Benchmarking, Scientific Research and Computational Applications, Weather Research and Forecasting Model, Python, AI and Machine Learning, Visualization, Outreach, Linux, Scratch.   

#### How often do you work with non-coding researchers?

I collaborate with non-coding researchers as much as I can when an opportunity arises. In my capacity, I am working on establishing a research network team involving individuals from historically disadvantaged South African institutions of higher learning. Through the Weather and Climate Modelling Group (WCMG) initiative, my goal is to co-supervise honours students, introducing them to numerical weather and climate methods, modelling and HPC. 

Additionally, in my community of Silverton, Pretoria, at a local church, once every year, I train learners from diverse backgrounds in computer hardware, Linux, and block-based programming from Scratch and Microbit, focusing on robotics. Through the Ulwazi Digital Innovators NPO, we are targeting skills development in coding, robotics and cybersecurity. 


{{< /spoiler >}}

{{< spoiler text="__:school: Tell us more about the organisation where you work?__" >}}

#### What is their primary objective?

The CHPC is South Africa’s National Scientific Computing facility with approximately 30 staff members. We service the research community in South Africa and Africa. We also have a few international programmes we support. Some of our continental and international highlights include supporting the SKA partner countries, the CERN ALICE Experiment and the SADC HPC Ecosystems Project. 

#### How many people in your organisation are involved in research software development (a thumbsuck is okay)?

Very few, maybe two or three individuals but I could be wrong. Currently, our focus is on deploying and supporting open-source and commercially available research software rather than developing it ourselves. In South Africa, there is a recognized lack of local expertise for undertaking such endeavours, despite many commercially available models not being configured for our specific regions. Thought leaders in South Africa are initiating discussions on how to foster software development tailored for Africa, with interdisciplinary collaboration being identified as a crucial requirement to achieve this goal.

{{< /spoiler >}}

{{< spoiler text="__:bulb: Where do you get training and support?__" >}}

#### Which communities of practice are you part of?

Before joining the CHPC, I actively participated in initiatives by the South African Society for Atmospheric Scientists (SASAS). At the CHPC, I am involved with the CHPC-Academy of Science South Africa (ASSAf) collaboration, which has a broad scope concerning communities of practice. Internationally, I have contributed to Research Software Engineer (RSE) initiatives at the ISC High Performance Conference in both 2023 and 2024. Recently, I also joined the RSSE-Africa community. 

#### What training has had an impact on your current career?

It has to be the European Centre for Medium-Range Weather Forecasts (ECMWF) MOOC on “Machine Learning in Weather and Climate”. 

{{< /spoiler >}}

{{< spoiler text="__:thought_balloon: Do you see yourself as an academic, researcher, software engineer, technician…? All of it? Something else? A mix of one or two terms?__ " >}}

At this stage of my career, I am a researcher, and scientific software engineering plays a central role in my work. Without a negative connotation, I consider myself a "Jack of all trades and master of one." In the context of the fourth industrial revolution (4IR), multidisciplinarity is considered an advantage.

{{< /spoiler >}}

{{< spoiler text="__:no_entry: What kind of barriers do you face in your work?__ " >}}

In South Africa, there is currently no formal university pathway for RSE skills development. Much of our progress in this field is based on practical experience. Additionally, there are limited opportunities for RSEs in Africa to pursue further development internationally due to our non-membership in international initiatives focused on advancing research software skills. I often encounter difficulties accessing international training opportunities. 

{{< /spoiler >}}

{{< spoiler text="__:heart_eyes: What part of your job do you enjoy the most?__" >}}

In my role, I enjoy exposure to complex, high-quality, computational scientific research. I draw parallels to Albert Einstein's time at the Swiss patent office, where exposure to new ideas likely influenced his formulations of scientific questions. Similarly, my current position dictates the type of research I choose to engage in. 

{{< /spoiler >}}

{{< spoiler text="__:sos: What would make things easier for you and support you in your work?__" >}}

Globalization, in its truest sense, where greater integration on a global scale would offer opportunities for RSEs in South Africa and across Africa to learn and excel within this dynamic field.

{{< /spoiler >}}

{{< spoiler text="__:telescope: What are you looking forward to this year?__" >}}

This year, my focus is on producing groundbreaking research results on Lesotho temperature and precipitation seasonality and long-term variability in collaboration with the Lesotho Meteorological Services. This project incorporates new tools, including machine learning, to improve weather and climate predictability in Lesotho. Additionally, delivering a keynote lecture at the RSE@ISC workshop and participating in the ISC Outreach BoF in May 2024 at ISC’24 were significant milestones for me, offering valuable insights and professional growth opportunities through interactions with an international audience.

Looking ahead, the CHPC National Conference in December promises to be exceptional with exciting plans such as the coding and robotics symposium, along with updates on cutting-edge computational research from around the world. Set in the beautiful city of Gqeberha, this conference will undoubtedly be a highlight of the year.


{{< /spoiler >}}

{{< spoiler text="__:speech_balloon: Please share the most helpful career advice you’ve received that you want to share with other Africans in similar roles.__" >}}

This is actually my own adage, though not unique, but I strongly believe that in the era of 4IR, multidisciplinarity is considered an advantage.

{{< /spoiler >}}




