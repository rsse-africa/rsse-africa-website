---
title: "Enabling Open Science Through Research Code: Insights from Episode 1 - Conversations with Researchers Who Code"

subtitle: 

# Summary for listings and search engines
summary: RSSE Africa partnered with the RSE Asia Association, Talarify, ReSA and AREN to host a 6-month virtual community conversation series focusing on Enabling Open Science through Research Code. Episode 1 took place on International RSE Day. Read more in our blog post.


# Link this post with a project
projects: []

# Date published
date: '2024-10-29T00:00:00Z'

# Date updated
lastmod: '2024-10-29T00:00:00Z'

# Is this an unpublished draft?
draft: false

# Show this page in the Featured widget?
featured: false

# Featured image
# Place an image named `featured.jpg/png` in this page's folder and customize its options here.
image:
  caption: ''
  focal_point: ''
  placement: 
  preview_only: true



authors:
  - Jyoti Bhogal
  - Mireille Grobbelaar
  - Anelda van der Walt
  - Saranjeet Kaur Bhogal
  

tags:
  - Open Science


categories:
  - Open Science
---

On __October 10, 2024__, the first episode of the ‘__Enabling Open Science Through Research Code__’  community conversation took place. The topic for the first episode was ‘__Conversations with Researchers Who Code__’ focusing on the pivotal role of research software in enabling open science. The event was hosted by __RSSE Africa__ and the __RSE Asia Association__. It aimed to bring together researchers from various disciplines to share their experiences of coding, collaboration, and the challenges they face as they navigate the world of open science. This episode was a part of the celebration for __International Research Software Engineer (RSE) Day__, bringing attention to the growing importance of RSEs in research.

### Session Overview

The session kicked off with facilitators Anelda van der Walt and Saranjeet Kaur welcoming participants and introducing the co-facilitators Jyoti Bhogal and Mireille Grobbelaar. This diverse team set the stage for a lively discussion with four distinguished speakers: [Dr. Biandri Joubert](https://www.linkedin.com/in/biandri-joubert-phd-8270763b/), [Dr. Gaurav Bhalerao](https://www.linkedin.com/in/gauravbhalerao/), [Mahmoud Abdelrazek](https://www.linkedin.com/in/razekmh/), and [Prasad Sutar](https://www.linkedin.com/in/prasadsutar11/). More than 90 people registered with around 30 actual participants from all across the world.

### Introductions & Open Science Insights

After a warm welcome, the conversation shifted to the core topic: __open science__ and the importance of research software and systems as described in the __UNESCO Open Science Recommendations__. The facilitators invited the speakers to share their academic backgrounds, work experiences, and current roles, giving the audience a sense of the various pathways that lead them into the world of research software coding.

Some of the key questions posed to the speakers included:
- What did you study, and where?
- Do you have postgraduate qualifications?
- What is your current role, and how does coding feature in your work?

This introduction provided a backdrop to understand how the speakers’ careers evolved, and how their coding skills were developed to support their research or that of others at the organisation where they work.

### Panel Discussion: The Life of a Researcher Who Codes

The heart of the session was a panel discussion where the speakers delved deeper into the realities of research coding. Some highlights from the discussion include:

- __Dr. Gaurav Bhalerao__ explained how coding forms an integral part of his day-to-day activities, particularly in structuring large datasets for neuroscience research. He also shared the challenges of balancing coding with research responsibilities.
- __Mahmoud Abdelrazek__ talked about his work in geospatial analysis and how his team collaborates on coding projects. He highlighted the collaborative nature of his work, where coding is not only a personal skill but also a shared responsibility within his research group.
- __Dr. Biandri Joubert__ touched on the challenges of being the sole programmer in her group and how she overcomes the feeling of isolation by participating in coding communities and seeking support from platforms like the Carpentries.
- __Prasad Sutar__ discussed his experience with developing software that is used by others and the importance of ensuring that his coding practices align with both the research objectives and software quality standards.

The panel also explored whether all researchers should learn to code. They agreed that even if a researcher doesn’t need to code regularly, learning basic coding concepts is valuable. This foundational knowledge enables researchers to read and understand code at a high level, helping them grasp the algorithms or logic applied to address specific problem statements. A  valuable analogy, comparing coding to medical training was suggested during the meetup.  Just as it’s beneficial for everyone to learn basic first-aid, it’s useful for researchers to grasp fundamental coding concepts. Some may advance to become paramedics, equivalent to those using coding more regularly, while a few specialize deeply - similar to neurosurgeons or expert programmers. What are your thoughts? Should coding become a core skill for all researchers, or is it more practical to foster collaboration with coding specialists when needed?

Other topics that were discussed included the amount of time spent coding, the collaborative nature of research software projects, and the availability of support and training for researchers.

### Learning and Community Support

An essential part of the discussion revolved around the resources and communities that help researchers improve their coding skills. Dr. Gaurav Bhalerao recommended resources such as Carpentries lessons and online coding tutorials like p5.js for those starting out. The speakers emphasized the importance of continuous learning and staying connected with the global community through platforms like RLadies, Quarto, and others. To know about more resources please see the section Resources and Links.

### Q&A and Wrap-Up

The session concluded with a Q&A where participants posed questions about the pain points of learning to code, transitioning into a coding role from other fields, and how to stay updated with coding trends while maintaining research quality. One insightful comment came from Dr. Biandri Joubert, who emphasized that code that runs doesn’t always mean it’s correct, highlighting the critical need for combining coding skills with sound statistical knowledge.

The facilitators wrapped up the event by sharing resources from partners, including RSSE Africa, RSE Asia, AREN, and ReSA, all of which provide platforms for collaboration, learning, and advocacy for the Research Software Engineering community.


### Resources and Links

To access the wealth of resources shared in the session, visit our [collection at Zenodo](https://zenodo.org/records/13946139).

For those who missed this enlightening conversation, a [__recording of Episode 1__](https://www.youtube.com/watch?v=UmSHhDVRGig) is available on the RSSE Africa website. 

### Upcoming Event

Please remember to [__register for Episode 2__](https://rsse.africa/events-rsse-africa/2024-11-14/) of the series. The conversation continues, highlighting the essential role of research software engineering in advancing open science globally.

### Learn More About Us

For more information and to join upcoming events, visit:
- __RSSE Africa__
  - Website: https://rsse.africa 
  - Sign up for our newsletter: https://talarify.us14.list-manage.com/subscribe?u=35d5db26d3b108b9ef9b9ac43&id=55e9f5a692
  - Join our LinkedIn group, where you can also share information with the broader community: https://www.linkedin.com/groups/12903402/ 

- __RSE Asia__
  - Website: https://rse-asia.github.io/RSE_Asia/ 
  - For the latest news, events, activities, and opportunities, follow us on our [LinkedIn page](https://www.linkedin.com/company/rse-asia-association/)
  - To join the RSE Asia community, please fill out our short [Community Membership Form](https://docs.google.com/forms/d/1XSxDaTJzcNyGeDYXyJNVg1TDCo7un18PLFNiK6_jL2g/edit)

- __AREN__
  - Website: https://africanrn.org/ 
  - Sign up: https://docs.google.com/forms/d/e/1FAIpQLSeeFkD5A4D9l6ncQWjKBiI-GqBOzL-JMe7Fx3ijUYEjHjDUoQ/viewform 


- __ReSA__
  - Website: https://www.researchsoft.org/
  - Sign up for the newsletter: https://www.researchsoft.org/news/ 
  - The Amsterdam Declaration on Funding Research Software Sustainability 
    - Become a signatory: https://adore.software/sign/ 