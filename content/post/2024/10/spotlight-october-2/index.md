---
title: "🇳🇬 October 2024: Meet Chioma Onyido"

subtitle: 

# Summary for listings and search engines
summary:  Welcome 👋 to our second October 2024 community member spotlight! In this edition we're introducing Chioma Onyido from Nigeria.

# Link this post with a project
projects: []

# Date published
date: '2024-10-29T11:00:00Z'


# Date updated
lastmod: '2024-10-29T11:00:00Z'

# Is this an unpublished draft?
draft: false

# Show this page in the Featured widget?
featured: false

# Featured image
# Place an image named `featured.jpg/png` in this page's folder and customize its options here.
image:
  caption: 'Image credit: Chioma Onyido'
  focal_point: ''
  placement: 2
  preview_only: false

authors:
  - Chioma Onyido

tags:
  - Community members
  - Bioinformatics
  - Nigeria

categories:
  - Spotlights
  - Nigeria
---

Our second spotlight for October shines on Chioma Onyido from Nigeria. Chioma is a Bioinformatics Research Assistant at Covenant University and a Machine Learning Engineer at HackBio.

## In short
---

- __Preferred name and surname__: Chioma Onyido
- __Affiliation__ (where do you work or study): Covenant University
- __Role__:   Bioinformatics Research Assistant
- __LinkedIn__: https://www.linkedin.com/in/chioma-onyido/ 
- __Google Scholar__: https://scholar.google.com/citations?user=OTw33EIAAAAJ&hl=en

---


{{< spoiler text="__:musical_score: What is your favourite song?__" >}}

:sound: [Born Tired](https://youtu.be/kLTwNhHVc7Q?si=-Z6tF9Rn7T705S04) – _Jhene Aiko_ 

{{< /spoiler >}}

{{< spoiler text="__:books: Can you describe your background and your current role?__" >}}

#### What did you study?

Bioinformatics, Covenant University, Nigeria

#### Do you have postgrad qualifications? In what area?

MSc. Biochemistry/ Molecular Biology

#### What is the title of your current role?

Bioinformatics Research Assistant

#### Give a one-sentence summary of what you do in this role

As a Bioinformatics Research Assistant, I analyze biological data, contribute to research publications, and collaborate on innovative bioinformatics projects. I'm also currently exploring how AI can be employed to develop more accurate and inclusive healthcare solutions.

{{< /spoiler >}}

{{< spoiler text="__:briefcase: Describe a typical workday... Expand specifically on the amount of time you spend coding__" >}}

#### How much time do you spend coding?

11 hours

#### How many projects do you work on?

2

#### Give some keywords that summarise the topics of your projects.

Polygenic Risk Scores, Artificial Intelligence, Precision Medicine, Nextflow, Breast Cancer, Underrepresented populations

#### How often do you work with non-coding researchers?

Often


{{< /spoiler >}}

{{< spoiler text="__:school: Tell us more about the organisation where you work?__" >}}

#### What is their primary objective?

CApIC-ACE focuses on building a critical mass of skilled professionals in Africa equipped with the knowledge and tools to address significant health challenges, particularly malaria and breast and prostate cancers.

{{< /spoiler >}}

{{< spoiler text="__:bulb: Where do you get training and support?__" >}}

#### Which communities of practice are you part of?

I am part of several communities of practice, such as Bioconductor, where I contribute as a biocurator and co-mentor, as well as the HackBio internship program where I’m working on machine learning projects to predict and classify cancer biomarkers and subtypes..

#### What training has had an impact on your current career?

Several training opportunities have had a significant impact on my career. Last year, I was awarded a travel fellowship to attend a Polygenic Risk Score (PRS) workshop in Kampala, which deepened my understanding of risk prediction models. Just last month, I also participated in a Galaxy training focused on machine learning for genomic data analysis, which has been instrumental in advancing my current projects.

{{< /spoiler >}}

{{< spoiler text="__:thought_balloon: Do you see yourself as an academic, researcher, software engineer, technician…? All of it? Something else? A mix of one or two terms?__ " >}}

I see myself as a blend of an academic, researcher, and bioinformatics engineer. Depending on the project, I often wear all these hats, bringing different aspects of my expertise to the table as needed to address various challenges.


{{< /spoiler >}}

{{< spoiler text="__:no_entry: What kind of barriers do you face in your work?__ " >}}

In my work, one of the biggest barriers is the lack of access to large, diverse genomic datasets, particularly for African populations. This limits the accuracy of predictive models and can skew research outcomes. Also, there are challenges in obtaining consistent funding and resources for advanced bioinformatics tools and infrastructure, which are essential for advancing my research.

{{< /spoiler >}}

{{< spoiler text="__:heart_eyes: What part of your job do you enjoy the most?__" >}}

What I enjoy most about my job is collaborating with different teams on innovative projects. Working together to solve complex biological problems using computational methods and seeing our efforts translate into impactful research has been a rewarding experience.  I also love the continuous learning aspect, as I get to explore and apply new techniques regularly- today I’m working on a gene expression project, another day I’m building a pipeline using nextflow- it keeps the work dynamic and exciting! 

{{< /spoiler >}}

{{< spoiler text="__:sos: What would make things easier for you and support you in your work?__" >}}

Increased funding for advanced bioinformatics tools and infrastructure would allow me to use the latest technologies more effectively. Secondly, ongoing professional development opportunities, such as workshops and training programs, would help me learn new/ hone my computational skills and stay updated with the latest advancements in the field, further supporting my work.

{{< /spoiler >}}

{{< spoiler text="__:telescope: What are you looking forward to this year?__" >}}

This year, I’m really looking forward to expanding my research on breast cancer risk prediction for African populations. I see so much potential for breakthroughs we can achieve in this area. I’m also looking forward to collaborating on more projects with various teams (both National and international) and gaining insights. I can't wait to attend more conferences and workshops to share my findings, learn from others in the field and build connections within the bioinformatics community. 


{{< /spoiler >}}

{{< spoiler text="__:speech_balloon: Please share the most helpful career advice you’ve received that you want to share with other Africans in similar roles.__" >}}

One of the most helpful pieces of career advice I’ve received is to never underestimate the power of sharing your unique perspective as an African researcher, your experiences and insights can contribute significantly to the global conversation in science. I also encourage networking and building connections within the community. “Opportunities are not in places but with people”. Engaging with others in your field can lead to some of the best opportunities. 

{{< /spoiler >}}




