---
title: "Leveraging HPC and RSE for addressing Africa's socio-economic and scientific challenges"

subtitle: 

# Summary for listings and search engines
summary: "In this post Mtheto Vuyo Sovara from the Centre for High Performance Computing in South Africa, shares his invited keynote at RSEHPC@ISC24 that took place on 16 May 2024, ISC, Hamburg: Enhancing the symbiosis between HPC and RSE communities."


# Link this post with a project
projects: []

# Date published
date: '2024-12-12T00:00:00Z'

# Date updated
lastmod: '2024-12-12T00:00:00Z'

# Is this an unpublished draft?
draft: false

# Show this page in the Featured widget?
featured: false

# Featured image
# Place an image named `featured.jpg/png` in this page's folder and customize its options here.
image:
  caption: ''
  focal_point: ''
  placement: 
  preview_only: true



authors:
  - Mthetho Vuyo Sovara
  

tags:
  - High performance computing 
  - Communities of practice


categories:
  - High performance computing 
  - Communities of practice
---

I had the privilege of representing the CHPC and South Africa at ISC, where I delivered a keynote titled “Leveraging HPC and RSE for addressing Africa's socio-economic and scientific challenges”. The opportunity came as a surprise, and I was deeply honoured to have been given this opportunity and platform. I wondered how the organizers, Stefanie Reuter and Matthew Archer from the University of Cambridge, came to know about me. It's likely they recognized me from my participation in a Birds of Feather Session the previous year at ISC, where my colleague Mr. Samuel Mathekga and I presented on democratizing HPC in Africa.

During that session, I made a choice to present half of my talk in isiXhosa, which is my mother tongue to an international audience. In a light-hearted moment, I jokingly threatened to quiz the audience on what I had just said, which elicited amusing reactions from everyone present. The underlying purpose of this exercise was to highlight the importance of translating HPC and programming content into native languages. In South Africa, this initiative was spearheaded by my colleague, Mr. Binyamin Barsch.

Initially, we relied on translation services, but we quickly recognized that this approach did not effectively capture the essence of certain words and concepts. Subsequently, we took on the task of translating the content ourselves. At CHPC, we are fortunate to have a diverse team representing various cultures across the country. Each of us has taken on the responsibility of translating the material into our respective home languages. Although this work is unpaid, we are driven by the belief in its profound benefit to others.

For my keynote at RSEHPC@ISC24 I decided to talk about “Leveraging HPC and RSE for addressing Africa's socio-economic and scientific challenges”. I remember my first participation at the ISC conference. It was in 2021 and I attended remotely. I asked a question that related to plans the international community had for Africa. Everyone on that panel looked confused and nobody could address the issue. I quickly realized that Africa needed to try by all means to stay abreast of international trends and self-sustain. In SA we have had an HPC system since for a long time now, but it was clear to me that we did not have an international footprint. 

Since then, everytime I attend an internal conference or workshop where I give a talk, I make sure to make the world know that Africa is doing great things in HPC. I will confidently say that we are internationally competitive. This is why I say we are internally competitive: 

- We have Digital Research Infrastructure: 
  - High-Performance Cluster
  - GPU Cluster
  - Localized Cloud Platform
  - Tier 2 Node facility (CERN)

- We have strategic international and local Collaborations
  - Large-Scale Science Projects
    - SKA – HPC Ecosystem Project 
    - CERN – ALICE Collaboration
  - Localized Cloud Projects
  - SARAO / SAWS

- We are Partnering with Emerging and Disruptive Technology Communities
  - Quantum Computing Community
  - Deep Learning, A.I and LM Community

Over the past seven years, the number of users on our cluster has grown substantially. This surge in demand for HPC resources in Africa indicates not only a rising interest in high-performance computing but also the increasing complexity of the problems we are tackling. Currently, our cluster, LENGAU, operates at 97% total CPU usage, and our Lustre storage file system consistently hovers around 80% capacity.

This upward trend underscores the critical role of HPC in advancing scientific and socio-economic research on the continent. The near-constant high utilization rates reflect our commitment to maximizing computational efficiency and resource availability. However, they also highlight the urgent need for continued investment in expanding our infrastructure to meet the escalating demands of our growing user base. By doing so, we can ensure that we remain at the forefront of addressing Africa's most pressing challenges through innovative and computationally intensive solutions.

![chpc cluster usage](2024-12-12-1.png)

### Who are these users?

The LENGAU cluster is provided free of charge to South African academic institutions, supporting a wide range of research and educational initiatives. Additionally, it is available to the public sector on a cost recovery model, ensuring that various governmental and non-profit organizations can leverage its capabilities. Our commitment to collaboration extends beyond national borders, as our African partners also have access to this valuable resource. This inclusive approach fosters a collaborative environment that drives scientific progress and innovation across the continent.

![chpc cluster usage](2024-12-12-2.png)


### Where are these users from? 

Here is a detailed breakdown of the usage by institution. Out of the 26 public universities in South Africa, 19 are active users of the CHPC. Among these, the University of KwaZulu-Natal (UKZN), Stellenbosch University, and the University of Pretoria (UP) are particularly prominent in their utilization of our resources. Interestingly, while the University of Limpopo (UL) does not have many active programs on the cluster, it consumes the most compute time. This significant usage is primarily attributed to the research group led by Professor Puti Ngoepe at UL.

![chpc cluster usage](2024-12-12-3.png)

### Which domain areas are being supported? 

This slide showcases the diverse scientific domains we support and their respective total usage in terms of hours utilized. The largest users of our HPC cluster are the domains of Material Science, Chemistry, and Earth Sciences. These fields have leveraged substantial computational resources, reflecting their intensive research needs and the critical role of HPC in advancing their scientific inquiries.

![chpc cluster usage](2024-12-12-4.png)

### Tangible results and outputs

Are the RSE and HPC communities in South Africa effectively supporting the local scientific community? The numbers speak for themselves, especially when considering the volume and impact of publications. This success rate is a testament to the invaluable contributions of Research Software Engineers at the CHPC, both past and present. Their expertise and dedication have been instrumental in enabling high-quality research and advancing scientific progress in South Africa.

![chpc cluster usage](2024-12-12-5.png)


The HPC user community can effectively address Africa's socio-economic and scientific challenges, thanks to the crucial role played by Research Software Engineers (RSEs). By bridging the gap between scientific research and software engineering, we empower researchers to concentrate on discovery and innovation. Forward-thinking researchers at some universities are increasingly recognizing the significance of this contribution and are integrating RSEs into their laboratories.

Scaled and optimized solutions provided by the HPC and RSE community have the potential not only to advance research but also to generate employment opportunities, thereby making a positive contribution to a country's GDP. This collaborative effort ensures that Africa remains at the forefront of scientific and technological development, driving progress across multiple sectors.

![chpc cluster usage](2024-12-12-6.png)

Without the support of RSEs and HPC, the South African Weather Service would face significant challenges in delivering timely and accurate weather forecasts to the citizens of South Africa. Weather and climate variability can profoundly impact both the micro and macro economy, influencing everything from daily activities to large-scale agricultural and industrial operations. The advanced computational capabilities provided by HPC, combined with the specialized expertise of RSEs, are essential for processing complex weather models and data, ensuring that forecasts are reliable and timely. This support is crucial for mitigating the economic impacts of weather-related events and enhancing the overall resilience of the nation's economy.

![chpc cluster usage](2024-12-12-7.png)

At the CHPC, we are fortunate to have a diverse group of South Africans, each bringing unique perspectives and solutions that drive democratized human capital development across our nation and the continent of Africa. Our approach to developing Research Software Engineers (RSEs) for HPC includes our annual Coding Summer School and HPC Winter School. These initiatives have a significant reach, encompassing all 26 South African universities and two national research institutes.

Our curriculum development is dynamic and responsive. We gather comprehensive information about our participants and tailor course outlines to align with their interests and skill development needs. Through quizzes, mini-projects, feedback surveys, and regular meetings with university champions, we continuously refine our approach to ensure maximum impact. This method ensures that our training is relevant, effective, and capable of addressing the evolving demands of the scientific and research communities.

![chpc cluster usage](2024-12-12-8.png)

Here are some of the sites where participants were actively involved in the Summer School Program activities:

![chpc cluster usage](2024-12-12-9.png)

In this slide, I presented one of our Human Capital Development initiatives aimed at empowering research scientists with RSE skills, particularly targeting SADC partners. It is important to note that in South Africa, there is currently no formal university pathway for RSE skills development. Much of our progress in this field is based on practical experience.

Additionally, there are limited opportunities for RSEs in Africa to pursue further development internationally. Our non-membership in international initiatives focused on advancing research software skills often results in difficulties accessing international training opportunities. Therefore, at ISC’24, I urged the international community not to overlook Africa’s needs in their planning efforts. Supporting our initiatives and addressing these gaps will significantly enhance our capacity to contribute to the global scientific community.

![chpc cluster usage](2024-12-12-10.png)

Despite these challenges, our students have achieved remarkable success, securing first place at the ISC Student Cluster Competition on no fewer than four occasions. This outstanding achievement highlights the dedication, skill, and resilience of our students, as well as the effectiveness of our training programs. It demonstrates that, with the right support and opportunities, our students can compete and excel on the international stage.

![chpc cluster usage](2024-12-12-11.png)

Furthermore, both in 2018 and 2022, the CHPC was recognized by HPC Wire for its leadership in workforce diversity and inclusion. This acknowledgment underscores our commitment to fostering an inclusive environment that values and celebrates diversity, enhancing our collective ability to drive innovation and excellence in high-performance computing.

![chpc cluster usage](2024-12-12-12.png)

At the local level, through our coding and robotics program content, we have made a significant impact in the development of a National Coding and Robotics Curriculum for the National Department of Basic Education. This initiative reflects our dedication to preparing for the Fourth Industrial Revolution (4IR) and nurturing the growth of a skilled workforce in coding, robotics, and artificial intelligence (AI). By contributing to this curriculum, we aim to equip students with essential skills that are crucial for success in the digital age, thereby supporting broader efforts to advance technological literacy and innovation across South Africa.

![chpc cluster usage](2024-12-12-13.png)

To address historical structural inequalities, we have embarked on a bold initiative to translate modern computer science and engineering terminology into Sesotho, Setswana, isiXhosa, and isiZulu. Staff members at CHPC who are fluent in these languages lead the effort to create new terms and documentation. This initiative not only contributes to the preservation and evolution of native languages but also bridges the gap between technological advancements and linguistic diversity. It reflects our commitment to inclusivity and empowerment, ensuring that all communities can fully participate and benefit from advancements in computer science and engineering.

![chpc cluster usage](2024-12-12-14.png)

Regarding the mutual benefit between the HPC and RSE communities for socio-economic and scientific advancement, it's crucial to emphasize that inclusivity serves as a driving force for innovation:

- __Diverse Perspectives__: Different viewpoints can spark novel ideas and approaches.
- __Inclusive Environments__: Encouraging the exchange of ideas and knowledge across diverse groups and disciplines fosters creativity and collaboration.
- __Effective Problem-Solving__: Inclusive teams, drawing on a wide range of skills and expertise, are better equipped to tackle complex challenges, leading to more innovative and effective solutions.

Looking ahead for South Africa's RSE and HPC community, it's important to consider the value of age diversity. Age diversity in HPC and RSE can significantly enrich our talent pool:

- __Innovation Through Diversity__: A diverse workforce fuels innovation by bringing varied perspectives and experiences to the table.
- __Enhanced Knowledge Sharing__: Different age groups can contribute unique insights and approaches, promoting continuous learning and growth.
- __Competitive Advantage__: Embracing diversity ensures that HPC and RSE organizations remain competitive and resilient in an ever-evolving field, adapting swiftly to technological advancements and global challenges.

By embracing inclusivity and age diversity, we can create a more dynamic and forward-thinking community, poised to lead and innovate in high-performance computing and research software engineering.

![chpc cluster usage](2024-12-12-15.png)

 







