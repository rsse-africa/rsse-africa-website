---
title: "Did you know about the FAIR for Research Software Principles?"
subtitle: 

# Summary for listings and search engines
summary: The FAIR for Research Software (FAIR4RS) Principles, introduced in 2022 are guiding a global shift in managing research software.

# Link this post with a project
projects: []

# Date published
date: '2024-03-20T00:00:00Z'

# Date updated
lastmod: '2024-03-20T00:00:00Z'

# Is this an unpublished draft?
draft: false

# Show this page in the Featured widget?
featured: false

# Featured image
# Place an image named `featured.jpg/png` in this page's folder and customize its options here.
image:
  caption: 'Image credit: Daniel S. Katz'
  focal_point: ''
  placement: 
  preview_only: false

authors:
  - Anelda van der Walt
  - Mireille Grobbelaar
  

tags:
  - Open Science
  - FAIR
  - ReSA

categories:
  - Open Science
  - FAIR
  - ReSA
---

When we hear the terms open science and FAIR principles (findability, accessibility, interoperability, and reusability), many of us immediately think of data or publications, but have you considered that these principles also apply to research software?

In 2022, the FAIR for Research Software (FAIR4RS) Working Group (WG)* released the FAIR4RS Principles, a transformative step resonating globally. These principles have revolutionised the management and maintenance of research software and inspired institutions worldwide to integrate them into their policies and practices. 

At RSSE Africa, we recognise these principles' critical role in advancing research software, particularly in the African research ecosystem.  We encourage our community members to empower themselves with knowledge surrounding FAIR4RS. The following two resources will help you gain more context:

1. **Foundational Nature article:** Gain insights into the original vision and objectives of the FAIR4RS Principles by reading the first article published in 2022 -
[Introducing the FAIR Principles for Research Software](https://www.nature.com/articles/s41597-022-01710-x)

2. **Two-year progress update blog post:** Discover the strides made since the release of FAIR4RS by visiting the ReSA website for updates from the last two years - [The FAIR for Research Software Principles after two years: an adoption update](https://www.researchsoft.org/blog/2024-03/)


*The FAIR4RS WG is an interdisciplinary international team which was jointly convened by the Research Software Alliance (ReSA), Future Of Research Communications and E-Scholarship (FORCE11), and the Research Data Alliance (RDA). 













