---
title: "ReSA: A global organisation with a footprint in Africa"
subtitle: 

# Summary for listings and search engines
summary: The Research Software Alliance (ReSA) is a non-profit organisation on a mission to bring research software communities together to collaborate on advancing the research software ecosystem. 

# Link this post with a project
projects: []

# Date published
date: '2023-03-04T00:00:00Z'

# Date updated
lastmod: '2023-03-04T00:00:00Z'

# Is this an unpublished draft?
draft: false

# Show this page in the Featured widget?
featured: false

# Featured image
# Place an image named `featured.jpg/png` in this page's folder and customize its options here.
image:
  caption: 'Image credit: Mohammed Ali'
  focal_point: ''
  placement: 
  preview_only: true

authors:
  - Anelda van der Walt
  - Michelle Barker
  - Paula Andrea Martinez

tags:
  - ReSA
  - International communities

categories:
  - ReSA
  - International communities
---

The [Research Software Alliance](https://www.researchsoft.org/) (ReSA) is a non-profit organisation on a mission to bring research software communities together to collaborate on advancing the research software ecosystem. The vision of ReSA is that research software is recognised and valued as a fundamental and vital component of research worldwide. 

In February 2023, ReSA appointed [Talarify](https://talarify.co.za) as the African ReSA Community Engagement Partner. Over the past eight years, Talarify has been involved in numerous community engagement initiatives locally and Research Software Engineering initiatives globally. In 2022, Talarify led the monthly meetups of the Research Software & Systems Engineers of Africa (RSSE Africa) community and will continue to do so in 2023.

As a ReSA Community Engagement Partner, Talarify will help grow awareness of and engagement with ReSA’s vision in their region. This and another new position in Asia have been made possible partly by [grant 2021-000000](https://doi.org/10.5281/zenodo.7275397) from the [Chan Zuckerberg Initiative](https://chanzuckerberg.com/) Donor-Advised Fund, an advised fund of the Silicon Valley Community Foundation. - read more about other ReSA community managers at <https://www.researchsoft.org/people/>.


> “We are excited about the opportunity to formally collaborate with ReSA to grow awareness of the importance 
> of research software and the people who create it in Africa and help create a platform to showcase African research software excellence.” 
>
> _Anelda van der Walt, Director, Talarify_

In the past year, ReSA facilitated opportunities for the research software community to come together to: 

- Co-develop the [FAIR for Research Software Principles](https://www.nature.com/articles/s41597-022-01710-x). 
- Collaborate with other funders via the [Research Software Funders Forum](https://www.researchsoft.org/funders-forum/). 
- Share diversity, equity, and inclusion best practices at _[Vive la différence - research software engineers](https://www.researchsoft.org/events/2022-04/)_. 
- Network at [Research Software Community Forums](https://www.researchsoft.org/events/2022-06/). 
- [Gather with global research software funders](https://zenodo.org/record/7384410#.ZAM5L9JByV6) to set the agenda for supporting sustainable research software. 
- Collaborate in [task forces](https://www.researchsoft.org/taskforces/). 

ReSA offers a wide range of opportunities to get involved, including

- Joining [task forces](https://www.researchsoft.org/taskforces/) focused on specific time-bound activities
- Receiving updates through their regular [newsletter](https://www.researchsoft.org/news/)
- Encouraging your organisation to become an [Organisational Member](https://www.researchsoft.org/membership/) or make a [donation](https://www.researchsoft.org/donate/)
- Helping to raise awareness of the importance of software in research through the use of [ReSA resources](https://www.researchsoft.org/resa-resources/)
- Connecting with other community members at [events](https://www.researchsoft.org/events/)
- Joining the [ReSA Slack](https://researchsoft.slack.com/join/shared_invite/zt-1flmrglww-SoWjAK_5TJyqLU_~Jx697w#/shared-invite/email) to share what’s happening in your community
- Attending the [Research Software Community Forum](https://www.researchsoft.org/events/2022-06/)
- Providing information on [new funding calls](https://forms.gle/r4Jw4swUd1SXigZc9) to the [Research Software Funding Opportunities](https://www.researchsoft.org/funding-opportunities/)
- [Contributing resources](https://www.researchsoft.org/contact/) and [guidelines](https://www.researchsoft.org/guidelines/); ideas for [task forces](https://www.researchsoft.org/taskforces/), events and [news](https://www.researchsoft.org/news/); or if you have any other ideas or events aligning to the ReSA vision, [get in touch](https://www.researchsoft.org/contact/).

We look forward to seeing you in the [ReSA community](https://www.researchsoft.org/)!