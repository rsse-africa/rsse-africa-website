---
title: "Exploring Global Research Software Communities 🌐 🔍"
subtitle: 

# Summary for listings and search engines
summary: "Discover newsletters and mailing lists from global and local spaces, gaining valuable insights into the vibrant landscape of RSE."

# Date published

# Date published
date: '2023-11-09T00:00:00Z'

# Date updated
lastmod: '2023-11-09T00:00:00Z'

# Is this an unpublished draft?
draft: false

# Show this page in the Featured widget?
featured: false

# Featured image
# Place an image named `featured.jpg/png` in this page's folder and customize its options here.
image:
  caption: 'Image credit: <a href="https://www.canva.com">Canva</a>'
  focal_point: ''
  placement: 2
  preview_only: true

authors:
  - Mireille Grobbelaar

tags:
  - Communities of Practice
  - RSE Communities
  - Global Collaboration
  - Knowledge Exchange

categories:
  - Newsletters
  - Knowledge Exchange

---

The term "Research Software Engineer" (RSE)  first emerged during the 2012 Collaborations Workshop of the Software Sustainability Institute. Since then, it gained recognition as a professional activity that focuses on the development of software in a research context. In Africa, we explicitely include people who develop and maintain research data and computing infrastructure. The second "S" in RSSE Africa refers to research systems engineers. 

It's fascinating to see how RSE communities have sprouted and evolved to provide platforms for knowledge exchange and skill enhancement. RSSE Africa is one of many similar spaces aimed at supporting RSSEs and generating useful networks.

Let's take a moment to explore newsletters and mailing lists from a selection of RSE communities. Being a member of a variety of communites of practice can significantly contribute to personal and professional development. We encourage you to explore these resources. 

### 🗺️ Global RSE communities:
---

- [Research Software Alliance](https://www.researchsoft.org/news/)
- [The United States Research Software Engineer Association](https://us-rse.org/newsletters/)
- [Society of Research Software Engineering](https://society-rse.org/announcements/)
- [The Software Sustainability Institute](https://www.software.ac.uk/news-and-blogs-hub)


### 🌍 Local RSSE-relevant communities
---

  - [UbuntuNet Alliance for Research and Education Networking](https://us9.campaign-archive.com/home/?u=495c8840d515b5f1b1840c24d&id=789e979df9)
  - [Deep Learning Indaba](https://deeplearningindaba.com/blog/)
  - [The National Institute for Theoretical and Computational Sciences (NITheCS)](https://nithecs.ac.za/newsletters/)
  - [Data Science for Health Discovery and Innovation in Africa (DS-I Africa)](https://dsi-africa.org/)
  - [Masakane](https://www.masakhane.io/)

---

If you know of more RSSE-relevant communities please let us know so that we can share it with the community. Looking forward continue this collaborative exchange of information! 🔄🤝


