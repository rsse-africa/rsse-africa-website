---
title: "🇰🇪 November 2023: Meet Pauline Karega"

subtitle: 

# Summary for listings and search engines
summary:  Welcome 👋 to our sixth community  member spotlight! In this month's edition we're introducing Pauline Karega from Kenya.

# Link this post with a project
projects: []

# Date published
date: '2023-11-26T00:00:00Z'


# Date updated
lastmod: '2023-11-26T00:00:00Z'

# Is this an unpublished draft?
draft: false

# Show this page in the Featured widget?
featured: false

# Featured image
# Place an image named `featured.jpg/png` in this page's folder and customize its options here.
image:
  caption: 'Image credit: Pauline Karega'
  focal_point: ''
  placement: 2
  preview_only: false

authors:
  - Pauline Karega

tags:
  - Community members
  - BHKi
  - University of Manchester
  - Bioinformatics
  - Kenya

categories:
 - Spotlights
 - Kenya
  
---

Our November spotlight shines on Pauline Karega from Kenya. Pauline is a PhD student at the University of Manchester and a cofounder of the Bioinformatics Hub of Kenya Initiative (BHKi). 


## In short
---

- __Preferred name and surname__: Pauline Karega
- __Affiliation__ (where do you work or study): University of Manchester and Bioinformatics Hub of Kenya Initiative (BHKi)
- __Role__:  PhD student and cofounder
- __Email__: karegapaul@gmail.com
- __LinkedIn__: https://www.linkedin.com/in/pauline-karega-20b72a145/
- __Twitter__: https://twitter.com/karegap?lang=en
- __ORCID ID__: 0000-0001-7974-048X


---


{{< spoiler text="__:musical_score: What is your favourite song?__" >}}

:sound: [I Still Haven't Found What I'm Looking For](https://www.youtube.com/watch?v=e3-5YC_oHjE) – _U2_ 

:sound: [Don't Stop Believin'](https://www.youtube.com/watch?v=1k8craCGpgs) – _Journey_ 


{{< /spoiler >}}

{{< spoiler text="__:books: Can you describe your background and your current role?__" >}}

#### What did you study?

I have a biochemistry undergraduate degree from Kenyatta University in Nairobi, Kenya, and an almost Bioinformatics masters degree from Nairobi University, Kenya. (Awaiting graduation) I  am currently a PhD student at the University of Manchester

#### What is the title of your current role?

Postgraduate researcher

#### Give a one-sentence summary of what you do in this role

I research how best to integrate environment and health data, comparing approaches in the global north and south 

{{< /spoiler >}}

{{< spoiler text="__:briefcase: Describe a typical workday... Expand specifically on the amount of time you spend coding__" >}}

#### How much time do you spend coding?

Approximately 4 hours

#### How many projects do you work on?

2 projects

#### Give some keywords that summarise the topics of your projects.

GIS, Sentinel, Landsat, respiratory illnesses, air pollution, health data, open data.

#### How often do you work with non-coding researchers?

Very often

{{< /spoiler >}}

{{< spoiler text="__:school: Tell us more about the organisation where you work?__" >}}

#### What is their primary objective?

Outside of my PhD, I work for Bioinformatics Hub of Kenya initiative, which aims to bridge the gap between early career and established researchers by building a cohesive community. We do this by running training events, doing outreaches in different universities, and running community meetups to support bioinformatics researchers and students.

#### How many people in your organisation are involved in research software development (a thumbsuck is okay)?

Many are involved in the research side, addressing different genomics questions, focusing more on data analysis than software development

{{< /spoiler >}}

{{< spoiler text="__:bulb: Where do you get training and support?__" >}}

#### Which communities of practice are you part of?

Open life science, Africa's talking, R ladies

#### What training has had an impact on your current career?

 A datacamp data science course I took at the beginning of my masters studies

{{< /spoiler >}}

{{< spoiler text="__:thought_balloon: Do you see yourself as an academic, researcher, software engineer, technician…? All of it? Something else? A mix of one or two terms?__ " >}}

I see myself as an academic and a researcher, for now. I hope I progress to a software engineer as I progress in my career. 

{{< /spoiler >}}

{{< spoiler text="__:no_entry: What kind of barriers do you face in your work?__ " >}}

Given that BHKi is an NGO, lack of funds is a big barrier to the work. It limits some of the progress we would like to make as an organization, like the support we would like to offer our members. We rely heavily on people volunteering their time, which makes it difficult to achieve some goals.

{{< /spoiler >}}

{{< spoiler text="__:heart_eyes: What part of your job do you enjoy the most?__" >}}

For my PhD, I really enjoy working with new datasets, and finding out the story the data is trying to tell. For BHKi, I would say organizing training programs and events, and finding people to collaborate with in the form of speakers, or trainers, you meet so many new and cool people doing amazing things.

{{< /spoiler >}}

{{< spoiler text="__:sos: What would make things easier for you and support you in your work?__" >}}

I would say funding to enable running a somewhat smooth program (given that there can be other challenges), halting operations that could create an impact because of funds kills morale. 

{{< /spoiler >}}

{{< spoiler text="__:telescope: What are you looking forward to this year?__" >}}

Christmas. I just moved to a different country so it will be the first time I’m spending Christmas away from family, but I have made friends and I can’t wait to spend the holidays with them.

{{< /spoiler >}}

{{< spoiler text="__:speech_balloon: Please share the most helpful career advice you’ve received that you want to share with other Africans in similar roles.__" >}}

Be open minded, be curious, and don’t be afraid to explore. You stumble upon the best things from the most unexpected places, conversations, and people. 

{{< /spoiler >}}




