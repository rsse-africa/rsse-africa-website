---
title: "A first for research software and systems development in Africa"
subtitle: 

# Summary for listings and search engines
summary: The Research Software Indaba will be hosted in Cape Town, South Africa  in May 2023.

# Link this post with a project
projects: []

# Date published
date: '2023-05-07T00:00:00Z'

# Date updated
lastmod: '2023-05-07T00:00:00Z'

# Is this an unpublished draft?
draft: false

# Show this page in the Featured widget?
featured: false

# Featured image
# Place an image named `featured.jpg/png` in this page's folder and customize its options here.
image:
  caption: ''
  focal_point: ''
  placement: 
  preview_only: true

authors:
  - Anelda van der Walt

tags:
  - Event
  - South Africa

categories:
  - Event
---

On 19 May 2023 leaders and funders in the research software and systems space will gather in person for the first time in Cape Town, South Africa to explore opportunities for local organisations and research communities.

> "Software has become an essential constituent of research, and research software is starting to be seen as an
> equal partner of research data in critical international policy documents such as [UNESCO](https://en.unesco.org/science-sustainable-future/open-science/recommendation) and the [OECD](https://www.oecd.org/sti/recommendation-access-to-research-data-from-public-funding.htm). 
> However, support and recognition of the importance of research software and the people who develop and 
> maintain it have failed to keep pace with the scale of the use of research software in research."
>
> https://www.researchsoft.org/funders-forum/

The Research Software Indaba will offer stakeholders an opportunity to meet with members from the global Research Software community, Michelle Barker _(director of the Research Software Alliance)_ and Prof Simon Hettrick _(deputy director at the UK Software Sustainability Institute)_ as well as local representatives from astronomy, bioinformatics, earth observation, publich health, high performance computing, language technology and eResearch amongst others. 

For more information, please visit the [website](https://rse-indaba.org).