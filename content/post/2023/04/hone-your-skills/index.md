---
title: "Honing your skills as Research Software Engineer"
subtitle: 

# Summary for listings and search engines
summary: One of the most common questions we've heard in the RSSE Africa community meetups are about becoming a research software engineer. In this post we share some pointers for growing your skills and networks. 

# Link this post with a project
projects: []

# Date published
date: '2023-04-4T00:00:00Z'

# Date updated
lastmod: '2023-04-04T00:00:00Z'

# Is this an unpublished draft?
draft: false

# Show this page in the Featured widget?
featured: false

# Featured image
# Place an image named `featured.jpg/png` in this page's folder and customize its options here.
image:
  caption: 'Honing your skills as research software engineer © 2023 by Anelda van der Walt, Caleb Kibet, Keaghan Brown, Nomalungelo Maphanga is licensed under CC BY 4.0'
  focal_point: ''
  placement: 
  preview_only: true

authors:
  - Anelda van der Walt
  - Caleb Kibet
  - Keaghan Brown
  - Nomalungelo Maphanga

tags:
  - Mentorship

categories:
  - Mentorship
---

![steps for honing your skills as RSE](featured.png)

Research Software Engineering (RSE) is not an all too familiar term on the African continent. In fact, most people working as research software engineers don’t have that job title but are employed as postdoctoral research fellows, data analysts, systems administrators, or researchers. Despite the novelty of the term, the continent boasts significant research software and infrastructure projects in the fields of bioinformatics, astronomy, language, and more.

RSEs work in a research environment and spend most of their time developing software or pipelines. We also include folks who develop and support research infrastructure in the RSSE Africa community.

The term RSE has gained much traction in the Global North over the past ten years. Read more about the movement that started in the UK in [various blog posts](https://www.software.ac.uk/author/simon-hettrick?page=1) by Prof Simon Hettrick, deputy director of the [Software Sustainability Institute](https://software.ac.uk). For our purposes, we define research software as follows:

> "Research Software includes source code files, algorithms, scripts, computational workflows and executables 
> that were created during the research process or for a research purpose. Software components (e.g., 
> operating systems, libraries, dependencies, packages, scripts, etc.) that are used for research but were not 
> created during or with a clear research intent should be considered software in research and not Research 
> Software. This differentiation may vary between disciplines. The minimal requirement for achieving 
> computational reproducibility is that all the computational components (Research Software, software used in 
> research, documentation and hardware) used during the research are identified, described, and made  
> accessible to the extent that is possible."
> 
> _[Source: https://doi.org/10.5281/zenodo.5504015]_


In our most recent RSSE Africa community meetup on 15 March, one of our community members asked, "How can I hone my skills as a research software engineer?”

We wanted to capture the discussion that followed to share with our community.


### Become part of a community 
---

RSSE Africa aims to share opportunities and resources with our community members. These include training and mentorship opportunities, calls for funding, webinars and conferences, publications, and more. By being part of the community, you will meet others on a similar journey who can share advice, connect you with others, and potentially become collaborators.

{{< cta cta_text="Sign up for the RSS	E Africa Newsletter" cta_link="https://talarify.us14.list-manage.com/subscribe?u=35d5db26d3b108b9ef9b9ac43&id=55e9f5a692" cta_new_tab="true" cta_alt_text="Join our next community call" cta_alt_link="https://docs.google.com/forms/d/e/1FAIpQLSd87Aa6Uyw4QhpvTh1RGMri7au96ewHvyaTvh-jfcGrhKNgRA/viewform" cta_alt_new_tab="true" >}}

The [Research Software Alliance](https://www.researchsoft.org/) and [Society of Research Software Engineering](https://society-rse.org/) are other communities to join.

### Sign up for a mentorship programme
---

There are numerous opportunities to hone your research software development skills. Don’t forget that research software is often open source, so look broader than just the research software community for mentorship opportunities. Some examples include:
- [Open Life Science Mentorship and Training Programme](https://openlifesci.org/) (not limited to life sciences despite the name!)
- [Deep Learning Indaba Mentorship Programme](https://deeplearningindaba.com/mentorship/)
- [Software Sustainability Institute’s Fellowships](https://www.software.ac.uk/programmes-and-events/fellowship-programme)
- [SSI Learning to Code mentorship programme](https://www.software.ac.uk/news/sign-become-mentor-ssi-learning-code-mentorship-programme)

### Explore open source internship opportunities
---

If you’re early career and can afford to take about three months out of studies or work, take a look at some of the programmes aimed at increasing diversity in open source. These may offer valuable opportunities to hone your skills as RSE. Some of them are even paid opportunities!
- [Outreachy](https://www.outreachy.org/) (full time, paid)
- [Google Summer of Code](https://summerofcode.withgoogle.com/)

### Contribute to open source projects
---

This step is probably most relevant to people already coding (probably in R and/or Python) and maybe are familiar with the command line via the shell. This means you are probably already using some open-source tools, for example, specific Python or R packages or command line tools.

Another way to grow your skills is to start contributing to the tools you are using. You can often contribute in various ways: from improving documentation, which may require less technical experience, contributing to issues identified with your favourite tool or package, to writing tutorials on how to use the tools. Many open-source tools are hosted and developed on version control platforms like Github or Gitlab. Developers and users often indicate where they need help with development through “issues.” This is a great place to start looking at the development process for the software of interest and identifying opportunities to contribute. All open source projects aren’t equal, but great projects typically have a guide on contributing, and easier issues are marked. If you’re unsure where to start, contact the project team to introduce yourself and ask how you can contribute. We will demonstrate this through the [Python Github repository](https://github.com/python/cpython).


#### How did I find this repository? 

I used my favourite search engine with the terms “Python” and “GitHub.” I looked at all the [repositories within the GitHub workspace for Python][1] and identified the repository I was looking for: https://github.com/python/cpython.   

#### What can I see in this repository?

At the top of the repo page is a list of all the files and folders, but if you scroll down, you’ll find a readme file with more information. Specifically, they have a guide on contributing:

![step1](step1.png)

#### How can I identify specific things to contribute to in this repo?

By clicking on the “Issues” tab at the top of the repo page, you will find different ways to contribute to previously identified bugs, enhancements, documentation improvements, etc.

![step2](step2.png)

In some cases (e.g. more established open-source projects or projects with a bigger emphasis on accessibility), the developer team may have added labels to the issues to make it easier to identify issues you can contribute to.

![step3](step3.png)

This may still look quite overwhelming. You can now click on the “Label” dropdown menu on the right side of the page to see a list of labels that have been created for this project. Try to find a label that seems like a good first issue to contribute to for a newcomer to the project, e.g. “Easy” or “Newcomer” or something like that.

![step4](step4.png)

Once you’ve selected the label of interest, the issues will be filtered, and you can browse through them to see if you would feel comfortable tackling anything.

![step5](step5.png)

Here we decide to click on Issue #102110, which was opened on 21 February 2023. We can see that it has something to do with documentation, so it probably won’t require coding per se. This seems like a safe place to test our hand at contributing to an open-source project. By clicking on the Issue, you will be redirected to more information and, in some cases, can follow the conversation around the issue to get more context.

![step6](step6.png)

In this instance, you can see that the person who created the issue was also invited to fix it. 

There are various benefits to contributing to open-source projects:
- Learn about the process of open-source software development (which can also be applied to proprietary software)
- Get to know the developers of the tools you use
- Developers and users get to know you 
- Get more intimate knowledge of how the software works
- Learn other skills (e.g. documentation, version control, online collaboration, debugging, etc.)
- Build up a portfolio to add to a resumé
- Much much more.

Some important considerations. 
- Some open-source projects have very small development teams and may not have clear contributing guidelines or processes. If the developers are kind, you could help them develop it by contributing. If the developers are not kind, move on to another project.
- You may need more time to contribute. It’s okay to lurk and get to know a project before jumping in. If you haven’t found a way to contribute or get involved, move on. There are many open-source projects that would love to have you.
- Many open-source projects also have discussion forums, mailing lists, Slack workspaces, conferences, or meetups you can join. Look out for those and attend them if you can.

### Start sharing your projects openly to build a portfolio
---

Once you are comfortable contributing to open source projects, you can start building a portfolio by making some of your projects open on platforms such as GitHub or GitLab. This will allow recruiters to review your GitHub repositories, validate your skills, and gain insight into your experience. 

----

We hope you found this post valuable. We’d love to hear from you as you embark on your journey to grow your RSE skills. Let’s continue the conversation in our monthly community discussions. You are also welcome to share your experiences through a blog post on the [RSSE Africa website](https://rsse.africa/post/).

If you know of other opportunities to hone RSE skills, please [let us know](https://rsse.africa/contact/)? We’d be happy to update this blog post or co-write another one.

















[1]: https://github.com/orgs/python/repositories

