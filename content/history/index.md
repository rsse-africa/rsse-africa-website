---
title: Our History
type: landing

sections:
  - block: experience
    content:
      title: Our History
      # Date format for experience
      #   Refer to https://wowchemy.com/docs/customization/#date-format
      date_format: 2006
      # Experiences.
      #   Add/remove as many experience `items` below as you like.
      #   Required fields are `title`, `company`, and `date_start`.
      #   Leave `date_end` empty if it's your current employer.
      #   Begin multi-line descriptions with YAML's `|2-` multi-line prefix.
      items:

        - title: The first RSE talk in Africa - James Hetherington @ eResearch Africa 2014
          company: 
          company_url: ''
          company_logo: 
          location: 
          date_start: '2014-11-21'
          date_end: '2014-11-21'
          description: |2-
              The first official RSE talk in Africa was given by Prof James Hetherington (then at University College London). James was one of the founders of the RSE movement in the UK around 2012. His talk was titled: [_The Craftsperson and The Scholar: Research Software Engineers and their place in the university_](http://www.eresearch.ac.za/presentations-2014-day-one) James' visit to South Africa co-incided with the first locally organised [Carpentries workshop](https://software-carpentry.org/blog/2014/12/cape-town-swc.html). 
        - title: Opportunities related to the RSE movement are shared with NWU staff and students
          company: 
          company_url: 'http://services.nwu.ac.za/it-news-eresearch/are-you-research-software-engineer'
          company_logo: 
          location: 
          date_start: '2016-04-01'
          date_end: '2016-04-01'
          description: |2-
              The NWU eResearch Initiative, led by Talarify, showcased [various opportunities](http://services.nwu.ac.za/it-news-eresearch/are-you-research-software-engineer) related to RSEs between 2015 - 2017.
        - title: A talk about Research Software Engineering and the UK Software Sustainability Institute to ASAUDIT
          company: 
          company_url: 'https://www.software.ac.uk/blog/2016-09-12-supporting-research-software-south-africa-and-africa'
          company_logo: 
          location: 
          date_start: '2016-04-01'
          date_end: '2016-04-01'
          description: |2-
              Aleksandra Pawlik (then at the UK Software Sustainability Institute) presented a talk related to RSEs at the Association of South African University Directors of Information Technology (ASAUDIT) Autumn General Institutional Meeting. ASAUDIT is now known as Higher Education Information Technology South Africa ([HEITSA](https://heitsa.ac.za/)). Aleksandra's talk was titled [_Five ways to support research software community_](https://www.software.ac.uk/blog/2016-09-12-supporting-research-software-south-africa-and-africa).
        - title: Talarify organises an RSE conversation as part of the NWU eResearch Initiative
          company: 
          company_url: ''
          company_logo: 
          location: 
          date_start: '2017-05-19'
          date_end: '2017-05-19'
          description: |2-
              This [2-hour session](https://www.quicket.co.za/events/29548-nwu-research-conversation-series-research-software-engineers/) focussed on the topic of Research Software Engineering and was offered to NWU researchers and professional staff. Speakers: Prof James Hetherington (then at University College London) and Aleksandra Pawlik (then at New Zealand eScience Infrastructure).
        - title: South Africa participates in global RSE survey created by the Software Sustainability Institute
          company: 
          company_url: ''
          company_logo: 
          location: 
          date_start: '2017-01-01'
          date_end: '2017-12-31'
          description: |2-
              In 2017 South Africa participated in the [International RSE Survey](https://github.com/softwaresaved/international-survey) run by the Software Sustainability Institute for the first time. With the help of Peter van Heusden (South African National Bioinformatics Institute) and Anelda van der Walt (Talarify), the survey questions were localised to speak to the South African context. The survey and data is [accessible online](https://www.software.ac.uk/blog/2018-03-12-what-do-we-know-about-rses-results-our-international-surveys).
        - title: South Africa participates in global RSE survey created by the Software Sustainability Institute
          company: 
          company_url: ''
          company_logo: 
          location: 
          date_start: '2018-03-01'
          date_end: '2018-12-31'
          description: |2-
              In 2018 South Africa participated in the [International RSE Survey](https://github.com/softwaresaved/international-survey) again. The survey and data is [accessible online](https://www.software.ac.uk/blog/2018-03-12-what-do-we-know-about-rses-results-our-international-surveys). Talarify supported the roll-out of the survey in Africa.
        - title: The SSI and Talarify supports community members from Namibia, Ehtiopia and South Africa/Sudan to participate in the RSE Group Leaders' meetup in London
          company: 
          company_url: ''
          company_logo: 
          location: 
          date_start: '2018-01-30'
          date_end: '2018-01-31'
          description: |2-
              In 2017 Simon Hettrick, Deputy Director of the SSI, lead a funding application to support travel for three African RSE community members to participate in the [first international RSE Group Leaders Meeting](http://researchsoftware.org/2018/04/23/int-rsel-workshop.html) in London. Participants included Jessica Upani (Namibia / Python Africa), Mesfin Diro (Addis Ababa Institute of Technology, Ethiopia) and Samar Elsheikh (Computational Biology division at the University of Cape Town, South Africa / Sudan).
        
        - title: The RSSE Africa forum is born
          company: 
          company_url: ''
          company_logo: 
          location: 
          date_start: '2019-11-01'
          date_end: '2019-11-01'
          description: |2-
              The Research Software and Systems Engineers of Africa (RSSE Africa) forum grew out of the experience of RSSEs (primarily at SANBI at the University of the Western Cape, in Cape Town, South Africa) in supporting colleagues across the continent. It was launched in November 2019 (at the ASBCB 2019 conference https://www.iscb.org/iscbafrica2019) as an informal skills sharing forum. Our aims are to raise awareness of the specific contributions that RSSEs make to research in Africa, to organise events for RSSEs on the continent and the provide a forum for sharing skills and opportunities within the community.

              The forum founders were [Peter van Heusden](../authors/peter_vanheusden/) and [Eugene de Beste](../authors/eugene_debeste/) from the SANBI, South Africa.

        - title: RSSE Africa gets a website and is further developed with mentorship from the Open Life Science community
          company: 
          company_url: ''
          company_logo: 
          location: 
          date_start: '2020-08-31'
          date_end: '2020-12-14'
          description: |2-
            The [Open Life Science Training and Mentorship Programme](https://openlifesci.org/) is a 16-week long mentorship and cohort-based training for people interested in applying open principles in their work and becoming Open Science ambassadors in their communities. In 2020 RSSE Africa was accepted as a project in the OLS programme. Project leads Peter van Heusden and Eugene de Beste aimed to use this opportunity to grow the community in terms of visibility and participation.

            This was at the height of the COVID pandemic, but despite the challenges faced, the team created a new website for RSSE Africa and much more during the 16-week programme.
 
            __Project title__: [Developing the Research Software and Systems Engineering Community to support Life Sciences in Africa](https://openlifesci.org/ols-2/projects-participants/#developing-the-research-software-and-systems-engineering-community-to-support-life-sciences-in-africa)

        - title: RSSE-Africa gets a dedicated channel on the UK RSE Society's Slack Workspace
          company: 
          company_url: ''
          company_logo: 
          location: 
          date_start: '2021-08-26'
          date_end: '2021-08-26'
          description: |2-
            The UK RSE Society hosts a [Slack workspace](https://forms.gle/eU6536gnRtyCVGyh9) for the global research software community. In 2021 the Society authorised the creation of a dedicated channel for the African RSSE community _#rsse-africa_. Please join us there!

        - title: Regular monthly virtual meetings kick off with support from the UK RSE community
          company: 
          company_url: ''
          company_logo: 
          location: 
          date_start: '2022-05-11'
          date_end: '2022-12-14'
          description: |2-
            In March 2022 Talarify committed to support the RSSE-Africa community by organising regular monthly meetups. The programme kicked off in May 2022. We had amazing support from the UK RSE community members who volunteered to join as presenters and share their knowledge and expertise about the global RSE movement, resources, opportunities, training, and more.

            Every alternative month we invited the African RSSE community to join us for a presentation followed by discussion and inbetween, we focussed our meetups on getting to know each other and contextualising the new knowledge to identify opportunities in the African context.

            View [our meetup programme here](https://events.zoom.us/eo/Am7GNyd1DZ-Cu5ffEKGd4slX5MvukxefuSxcdDqYYSKaU8QECL2S~AggLXsr32QYFjq8BlYLZ5I06Dg).

        - title: Talarify augments the African Research Software Stakeholder Mapping in collaboration with the Research Software Alliance (ReSA)
          company: 
          company_url: ''
          company_logo: 
          location: 
          date_start: '2022-09-19'
          date_end: '2023-01-16'
          description: |2-
            In 2022 [ReSA](https://www.researchsoft.org/) commisioned a survey of research software organisations, projects, communities and funders [in the Global South](https://www.researchsoft.org/blog/2022-10/). The data and report is available on [Zenodo](https://zenodo.org/record/7179892#.Y9YQxdJByV4). For this mapping exercise, a number of African RSE community members were recruited as consultants to ensure the project could build on local knowledge as RSE is not yet a term widely known on the continent.

            To build on the momentum from the mapping project by ReSA and the consultants, and to incorporate additional knowledge of the local landscape, Talarify submitted another project to the Open Life Science programme, this time with a focus on finding local stakeholders and creating an interactive map where both ReSA data and the new information from the latest mapping exercise would be displayed.

            __Project title__: [Mapping the RSSE landscape in Africa](../mapping/)

            __Project outcomes__:
            1. More than 150 additional African research software stakeholders were identified and included in the dataset;
            2. A new form was developed to capture information from the African community;
            3. An interactive R Shiny web app was developed to display the data on a map; and
            4. A lot of new awareness was created about the global RSE movement and RSSE-Africa in particular. Talarify showcased the mapping project at the World Science Forum in December 2022 in Cape Town, South Africa.    

        - title: The RSSE-Africa monthly community newsletter is launched
          company: 
          company_url: ''
          company_logo: 
          location: 
          date_start: '2022-12-14'
          date_end: '2022-12-14'
          description: |2-
            The first RSSE Africa newsletter is created by the Talarify team and shared with our community via Mailchimp. A total of 127 people is subscribed to receive the newsletter!

            [__Read the first newsletter now!__](https://mailchi.mp/talarify.co.za/celebrating-africas-research-software-community)

        - title: Community member spotlight features are launched as part of the newsletter and website
          company: 
          company_url: ''
          company_logo: 
          location: 
          date_start: '2023-01-31'
          date_end: '2023-01-31'
          description: |2-
            Over the past few years a number of international RSE communities have launched features on their websites showcasing the different careerpaths, job titles, roles, and more of their community members through interview-style posts. See for example [RSE Australia](https://rse-aunz.github.io/posts/), and the [Society of Research Software Engineering](https://society-rse.org/careers/case-studies/).

            In order to grow more visibility of African RSSEs, provide some value for our community members, and to have an opportunity to get to know our members better, we launched the [RSSE Africa Community Spotlights](../spotlights/) in January 2023.

        - title: ReSA appoints African Community Engagement Partner
          company: 
          company_url: ''
          company_logo: 
          location: 
          date_start: '2023-02-07'
          date_end: '2024-02-06'
          description: |2-
            In February 2023, ReSA appointed Talarify as the African Community Engagement Partner. In this role Talarify will help grow awareness of and engagement with ReSA’s vision in the region. This and another new position in Asia have been made possible partly by grant 2021-000000 from the Chan Zuckerberg Initiative Donor-Advised Fund, an advised fund of the Silicon Valley Community Foundation. Read more about ReSA community managers at https://www.researchsoft.org/people/.


        - title: Kim Martin from RSE@SUN embarks on a roadtrip to learn more about UK RSE groups 
          company: 
          company_url: ''
          company_logo: 
          location: 
          date_start: '2023-05-31'
          date_end: '2023-06-17'
          description: |2-
            The aims of this study are to provide useful information to the RSE Community on the subject of how to establish and develop RSE Groups, through comparing and contrasting the histories, strategies, and operational realities of a selection of UK-based RSE Groups.


            This information will primarily be gathered by Dr Kim Martin during the course of the ‘RSE Roadtrip’ (from 5th May to 16th June 2023, with financial support from the Software Sustainability Institute) mainly via interviews with individual RSEs (as circumstances allow, given voluntary participation of RSEs).

            Kimberly C. Martin. (2023). Research Software Engineering Groups in the UK; Origins, Organisational Context, and Practices - 'RSE Roadtrip' Planning Document (v0.1). Zenodo. https://doi.org/10.5281/zenodo.7852661

        - title: First Research Software Indaba takes place in Cape Town, South Africa
          company: 
          company_url: ''
          company_logo: 
          location: 
          date_start: '2023-05-19'
          date_end: '2023-05-19'
          description: |2-
            Talarify hosted the first in-person [Research Sofware Indaba](https://rse-indaba.org) in partnership with the Research Software Alliance (ReSA) and RSSE Africa. The Indaba aimed to spotlight global and local research software ecosystem developments and explore how these developments may impact and benefit local organisations and communities.

            The report is available at https://doi.org/10.5281/zenodo.7980634 and a write-up about the event can be read on the [Talarify website](https://www.talarify.co.za/2023/05/29/driving-sustainable-research-software-and-systems-insights-from-the-first-research-software-indaba-in-africa/).

        - title: Presentation about RSSE-Africa at RSECon23 (virtually)
          company: 
          company_url: ''
          company_logo: 
          location: 
          date_start: '2023-09-06'
          date_end: '2023-09-06'
          description: |2-
            **Title:** What have we learned from nine years of RSE advocacy in Africa?<br>
            **Presenter:** Anelda van der Walt<br>
            **Co-presenter:** Peter van Heusden<br>
            **Session:** International RSE Community Building: learn about the fundamentals of community building and join the launch of a 'Howto Guidebook on international RSE Community Building<br>
            **Slides:** https://zenodo.org/doi/10.5281/zenodo.10013745<br>
            **Conference website:** https://rsecon23.society-rse.org/<br>
            **Detailed programme**: https://virtual.oxfordabstracts.com/#/e/RSECon23/program

        - title: Presentation about RSSE-Africa at ZA-REN Week 2023 in Cape Town, South Africa
          company: 
          company_url: ''
          company_logo: 
          location: 
          date_start: '2023-09-20'
          date_end: '2023-09-20'
          description: |2-
            **Title:** Growing awareness of the importance of research software and the people who develop it<br>
            **Presenter:** Anelda van der Walt (Talarify)<br>
            **Co-presenter:** Dr. Michelle Barker (ReSA)<br>
            **Session:** Cloudy with a chance of research<br>
            **Slides:** https://zenodo.org/doi/10.5281/zenodo.10013667<br>
            **Conference website:** https://events.tenet.ac.za/event/33/

        - title: NITheCS hosts a virtual mini-school focussed on Research Software Engineering
          company: 
          company_url: ''
          company_logo: 
          location: 
          date_start: '2023-10-04'
          date_end: '2023-10-25'
          description: |2-
            This series of talks celebrated International Research Software Engineering (RSE) Day, declared to be on the second Thursday of October by the International RSE Society. The talks aimed to give the audience a comprehensive understanding of the RSE role and the value RSEs can offer to the research environment. It was aimed at new graduates and early-career researchers who may wish to consider RSE as a future career direction, as well as policy makers within research institutions and funding agencies who may wish to develop an understanding of the importance of RSE in the research context, and how and why RSEs should be developed and supported.   
            
            The mini-school was co-organised by Dr. Kim Martin from Stellenbosch.

            More information: https://nithecs.ac.za/wp-content/uploads/2023/10/NITheCS-Mini-school-Oct-23.pdf

            Recordings: https://www.youtube.com/playlist?list=PLE9Qrf4CJnRHQqxnnQDdS2nSs_XV5HoEG


        - title: ReSA Reappoints African Community Engagement Partner
          company: 
          company_url: ''
          company_logo: 
          location: 
          date_start: '2024-04-01'
          date_end: '2025-03-31'
          description: |2-
            In March 2024, ReSA reappointed Talarify as the African Community Engagement Partner. In this role, Talarify will continue to expand awareness and foster engagement with ReSA’s vision across the region. This renewal is made possible by ReSA's successful funding bid to the Alfred P. Sloan Foundation, enabling continued community-led collaborations on common challenges in the research software ecosystem throughout 2024-26.

            ReSA’s mission aligns with the goals of the Sloan Foundation’s Technology program, "Better Software for Science," which aims to develop practices, norms, and institutions that promote the development and adoption of discovery-enhancing software.

            Read the ReSA's full proposal at https://zenodo.org/records/10927376


        - title: Community Showcase Feature in French 
          company: 
          company_url: ''
          company_logo: 
          location: 
          date_start: '2024-04-27'
          date_end: '2024-04-27'
          description: |2-
            In April 2024 we featured Richard Dushime in our community spotlights. Richard is originally from the Democratic Republic of Congo based in Francophone Africa. Richard was kind enough to translate his spotlight article to French to make it accessible to a larger proportion of our community.

            Read Richard's spotlight in [English](../post/2024/04/spotlight-april/) and [French](../post/2024/04/spotlight-april-french/).

        - title: RSSE Africa meets Africa-Oxford Fellows 
          company: 
          company_url: ''
          company_logo: 
          location: 
          date_start: '2024-06-24'
          date_end: '2024-06-24'
          description: |2-
            RSSE Africa participated in the AfOx-OxRSE half-day workshop, where we shared insights about our community and its mission.

            The Africa Oxford (AfOx) Initiative is a cross-university platform dedicated to fostering partnerships between the University of Oxford and African institutions. The AfOx Visiting Fellowship Programme supports African scholars across various disciplines. 

            The Oxford Research Software Engineering Group (OxRSE) is a central group at the University of Oxford that provides software development and consultation for research projects throughout the University. 

            Read more about the workshop in [our blog post](../post/2024/06/AfOx-OxRSE-workshop/).
            
        - title: First Newsletter Fully Available in English and Français!
          company: 
          company_url: ''
          company_logo: 
          location: 
          date_start: '2024-06-28'
          date_end: '2024-06-28'
          description: |2-
            Richard Dushime joined the RSSE Africa organising team in June and helped us to translate this month's newsletter to French!

            We hope this will greatly benefit our French-speaking African community members as well as the broader international French-speaking community!

            Read our first newsletter available in [English and French online](https://us14.campaign-archive.com/?u=35d5db26d3b108b9ef9b9ac43&id=9b8c92f811).

        - title: RSSE Africa Invited to Participate in International Proposal Focussing on Capacity Development
          company: 
          company_url: ''
          company_logo: 
          location: 
          date_start: '2024-08-09'
          date_end: '2024-08-09'
          description: |2-
            Researchers from the UK reaches out to invite RSSE Africa to collaborate on an STFC proposal related to Physics and Research Software Engineering.
            
        - title: RSSE Africa Invited to Join International Council of RSE Associations Meetings
          company: 
          company_url: ''
          company_logo: 
          location: 
          date_start: '2024-08-12'
          date_end: '2024-08-12'
          description: |2-
            Earlier in 2024 ReSA was appointed as the convener of the [International Council of RSE Associations](https://researchsoftware.org/). 
            
            RSSE Africa has been invited as an observer to join regular meetings of the Council.


          
    design:
      # Choose how many columns the section has. Valid values: '1' or '2'.
      columns: '1'
---


