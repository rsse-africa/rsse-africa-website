---
# Display name
title: Jyoti Bhogal

# Is this the primary user of the site?
superuser: false

# Role/position/tagline
role: "Statistician"

user_groups: ["Programme Committee"]

# Organizations/Affiliations to show in About widget

organizations:  
  - name: RSE Asia Association
  - url: https://www.linkedin.com/in/jyoti-bhogal

# Short bio (displayed in user profile at end of posts)
bio: 
  

# Interests to show in About widget
interests:
  - Jyoti is a trained Statistician, a Software Quality Engineer, and a Data Modeler. She is proficient in R, Python, JavaScript. Jyoti is the co-founder of RSE Asia and currently works with ReSA as the Asian Community Engagement Partner. She has been working in Clinical Sciences since 2020; and has developed expertise in principles of good software development life cycle, and Clinical Trials R&D cycle. She is an open source enthusiast, and is keen on giving the young minds the exposure to the unconventional yet essential roles in tech.


# Education to show in About widget
education:


# Social/Academic Networking
# For available icons, see: https://sourcethemes.com/academic/docs/page-builder/#icons
#   For an email link, use "fas" icon pack, "envelope" icon, and a link in the
#   form "mailto:your-email@example.com" or "/#contact" for contact widget.
social:
- icon: linkedin
  icon_pack: fab
  link: https://www.linkedin.com/in/jyoti-bhogal/
- icon: envelope
  icon_pack: fa
  link: "mailto:bhogaljyoti1@gmail.com"



# Link to a PDF of your resume/CV.
# To use: copy your resume to `static/media/resume.pdf`, enable `ai` icons in `params.toml`, 
# and uncomment the lines below.
# - icon: cv
#   icon_pack: ai
#   link: media/resume.pdf

# Enter email to display Gravatar (if Gravatar enabled in Config)
email: ""

# Highlight the author in author lists? (true/false)
highlight_name: false
---

