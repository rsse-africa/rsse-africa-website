---
# Display name
title: Chioma Onyido

# Is this the primary user of the site?
superuser: false

# Role/position/tagline
role: "Bioinformatics Engineer"

user_groups: ["Episode 3 Speakers"]

# Organizations/Affiliations to show in About widget

organizations:  
  - name: Bioconductor & Covenant University
  - url: 

# Short bio (displayed in user profile at end of posts)
bio: |

# Interests to show in About widget
interests: 
 - <br> Chioma is a seasoned biocurator and bioinformatician with a background in biochemistry, who transitioned into Bioinformatics to explore how computational tools and data science can solve real-world health challenges. Her work spans using Bioconductor and open-source tools to analyze complex biological datasets and develop predictive models, aiming to improve disease risk prediction for underrepresented populations. She enjoys making research more accessible by creating well-documented, reproducible code and sharing bioinformatics resources with the community. 


# Education to show in About widget
education:


# Social/Academic Networking
# For available icons, see: https://sourcethemes.com/academic/docs/page-builder/#icons
#   For an email link, use "fas" icon pack, "envelope" icon, and a link in the
#   form "mailto:your-email@example.com" or "/#contact" for contact widget.
social:
- icon: user
  icon_pack: fa
  link: https://www.linkedin.com/in/chioma-onyido/
- icon: envelope
  icon_pack: fa
  link: mailto:chiomabonyido@gmail.com

# Link to a PDF of your resume/CV.
# To use: copy your resume to `static/media/resume.pdf`, enable `ai` icons in `params.toml`, 
# and uncomment the lines below.
# - icon: cv
#   icon_pack: ai
#   link: media/resume.pdf

# Enter email to display Gravatar (if Gravatar enabled in Config)
email: ""

# Highlight the author in author lists? (true/false)
highlight_name: true



---

