---
# Display name
title: Adeyinka Oresanya
# Is this the primary user of the site?
superuser: false

# Role/position/tagline
role: "<br> Software Developer"

user_groups: ["Episode 3 Speakers"]

# Organizations/Affiliations to show in About widget

organizations:  
  - name: Community Health Analytics in Open Source Software (CHAOSS)
  - url: 

# Short bio (displayed in user profile at end of posts)
bio: |

# Interests to show in About widget
interests: 
 - <br> Adeyinka Oresanya is a multidisciplinary software developer and open-source maintainer with a strong foundation in research. She holds a PhD in Agricultural Extension and Rural Development, combining a unique blend of academic rigour and practical software engineering skills to lead impactful projects. As the Badging Lead for the DEI Event Badging Program at CHAOSS, she plays a key role in advancing diversity, equity, and inclusion within open-source communities and events. Adeyinka also leads the CHAOSS Africa Developers’ Focus Group, where she drives initiatives aimed at fostering the growth of open-source developer communities across Africa.


# Education to show in About widget
education:


# Social/Academic Networking
# For available icons, see: https://sourcethemes.com/academic/docs/page-builder/#icons
#   For an email link, use "fas" icon pack, "envelope" icon, and a link in the
#   form "mailto:your-email@example.com" or "/#contact" for contact widget.
social:
- icon: user
  icon_pack: fa
  link: https://linkedin.com/in/adeyinkaoresanya
- icon: envelope
  icon_pack: fa
  link: adeyinkaoresanya@gmail.com

# Link to a PDF of your resume/CV.
# To use: copy your resume to `static/media/resume.pdf`, enable `ai` icons in `params.toml`, 
# and uncomment the lines below.
# - icon: cv
#   icon_pack: ai
#   link: media/resume.pdf

# Enter email to display Gravatar (if Gravatar enabled in Config)
email: ""

# Highlight the author in author lists? (true/false)
highlight_name: true



---

