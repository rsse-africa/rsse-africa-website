---
# Display name
title: Joel Nitta
# Is this the primary user of the site?
superuser: false

# Role/position/tagline
role: "<br> <br> <br> <br> Associate Professor"

user_groups: ["Episode 4 Speakers"]

# Organizations/Affiliations to show in About widget

organizations:  
  - name: Chiba University, Japan 
  - url: 

# Short bio (displayed in user profile at end of posts)
bio: |

# Interests to show in About widget
interests: 
 - <br> I am an evolutionary biologist with a focus on fern ecology and evolution at Chiba University, Japan. I use, create, and teach tools to enable open, reproducible science. My programming language of choice is R. I obtained my BA from UC Berkeley, MS from the University of Tokyo, and PhD from Harvard University. I am a certified instructor for The Carpentries and am active in communities of practice including ROpenSci, The Carpentries, and RSE Asia.


# Education to show in About widget
education:


# Social/Academic Networking
# For available icons, see: https://sourcethemes.com/academic/docs/page-builder/#icons
#   For an email link, use "fas" icon pack, "envelope" icon, and a link in the
#   form "mailto:your-email@example.com" or "/#contact" for contact widget.
social:
- icon: user
  icon_pack: fa
  link: https://www.joelnitta.com
- icon: envelope
  icon_pack: fa
  link: "mailto:joelnitta@gmail.com"
- icon: hashtag
  icon_pack: fa
  link: https://bsky.app/profile/joelnitta.com

# Link to a PDF of your resume/CV.
# To use: copy your resume to `static/media/resume.pdf`, enable `ai` icons in `params.toml`, 
# and uncomment the lines below.
# - icon: cv
#   icon_pack: ai
#   link: media/resume.pdf

# Enter email to display Gravatar (if Gravatar enabled in Config)
email: ""

# Highlight the author in author lists? (true/false)
highlight_name: true



---

