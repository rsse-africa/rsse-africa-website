---
# Display name
title: Dr Biandri Joubert

# Is this the primary user of the site?
superuser: false

# Role/position/tagline
role: "<br> Postdoctoral Research Fellow"

user_groups: ["Episode 1 Speakers"]

# Organizations/Affiliations to show in About widget

organizations:  
  - name: University of Witwatersrand, South Africa
  - url: 

# Short bio (displayed in user profile at end of posts)
bio: |

# Interests to show in About widget
interests: 
 - <br> I am a post-doctoral research fellow- and I specialize in import and export law, specifically the import and export of agricultural products. I am particularly interested in applying R programming to legal and interdisciplinary research and I mostly do so working with text and trade statistics. 


# Education to show in About widget
education:


# Social/Academic Networking
# For available icons, see: https://sourcethemes.com/academic/docs/page-builder/#icons
#   For an email link, use "fas" icon pack, "envelope" icon, and a link in the
#   form "mailto:your-email@example.com" or "/#contact" for contact widget.
social:
- icon: linkedin
  icon_pack: fab
  link: https://www.linkedin.com/in/biandri-joubert-phd-8270763b/
#- icon: building-user
#  icon_pack: fa
#link: https://www.ndcn.ox.ac.uk/team/gaurav-bhalerao
#- icon: user
#  icon_pack: fa
#  link: https://orcid.org/0000-0002-3325-3413
- icon: envelope
  icon_pack: fa
  link: mailto:biandrijoubert@gmail.com

# Link to a PDF of your resume/CV.
# To use: copy your resume to `static/media/resume.pdf`, enable `ai` icons in `params.toml`, 
# and uncomment the lines below.
# - icon: cv
#   icon_pack: ai
#   link: media/resume.pdf

# Enter email to display Gravatar (if Gravatar enabled in Config)
email: 

# Highlight the author in author lists? (true/false)
highlight_name: true



---

