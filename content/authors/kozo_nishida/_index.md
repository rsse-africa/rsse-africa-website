---
# Display name
title: Kozo Nishida

# Is this the primary user of the site?
superuser: false

# Role/position/tagline
role: "<br> Project Researcher"

user_groups: ["Episode 2 Speakers"]

# Organizations/Affiliations to show in About widget

organizations:  
  - name: Tokyo University of Agriculture and Technology
  - url: 

# Short bio (displayed in user profile at end of posts)
bio: 

# Interests to show in About widget

interests: 
 - <br> I worked as a technical staff member at RIKEN for 11 years, during which I became familiar with research software communities such as PyData and Bioconductor, and released several research software packages. I am interested in contributing to the open science community.


# Education to show in About widget
education:


# Social/Academic Networking
# For available icons, see: https://sourcethemes.com/academic/docs/page-builder/#icons
#   For an email link, use "fas" icon pack, "envelope" icon, and a link in the
#   form "mailto:your-email@example.com" or "/#contact" for contact widget.
social:
# - icon: linkedin
#   icon_pack: fab
#   link: https://twitter.com/aneldavdw
# - icon: building-user
#   icon_pack: fa
#   link: https://www.ndcn.ox.ac.uk/team/gaurav-bhalerao
- icon: user
  icon_pack: fa
  link: https://orcid.org/0000-0001-8501-7319
# - icon: envelope
#   icon_pack: fa
#   link: mailto:gvbhalerao591@gmail.com

# Link to a PDF of your resume/CV.
# To use: copy your resume to `static/media/resume.pdf`, enable `ai` icons in `params.toml`, 
# and uncomment the lines below.
# - icon: cv
#   icon_pack: ai
#   link: media/resume.pdf

# Enter email to display Gravatar (if Gravatar enabled in Config)
email: 

# Highlight the author in author lists? (true/false)
highlight_name: true



---

