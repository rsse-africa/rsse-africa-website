---
# Display name
title: Mars Lee
# Is this the primary user of the site?
superuser: false

# Role/position/tagline
role: "<br> Technical Illustrator"

user_groups: ["Episode 3 Speakers"]

# Organizations/Affiliations to show in About widget

organizations:  
  - name: NumPy & Open Source Design
  - url: 

# Short bio (displayed in user profile at end of posts)
bio: |

# Interests to show in About widget
interests: 
 - <br> Mars Lee is a Technical Illustrator that makes byte-sized comics that explain technical topics! She uses art, an under-represented skillset in open source, to get scientists contributing. From contributing back to libraries they use, she hopes scientists share their own research code and software. She is active in the Scientific Python space, with contributions to NumPy, JupyterLab, scikit-learn and more. Her most recent works are the ‘How to Contribute to NumPy’ comics - https://heyzine.com/flip-book/3e66a13901.html


# Education to show in About widget
education:


# Social/Academic Networking
# For available icons, see: https://sourcethemes.com/academic/docs/page-builder/#icons
#   For an email link, use "fas" icon pack, "envelope" icon, and a link in the
#   form "mailto:your-email@example.com" or "/#contact" for contact widget.
social:
- icon: user
  icon_pack: fa
  link: https://www.linkedin.com/in/mars-lee/
- icon: twitter
  icon_pack: fab
  link: https://x.com/marsbarlee

# Link to a PDF of your resume/CV.
# To use: copy your resume to `static/media/resume.pdf`, enable `ai` icons in `params.toml`, 
# and uncomment the lines below.
# - icon: cv
#   icon_pack: ai
#   link: media/resume.pdf

# Enter email to display Gravatar (if Gravatar enabled in Config)
email: ""

# Highlight the author in author lists? (true/false)
highlight_name: true



---

