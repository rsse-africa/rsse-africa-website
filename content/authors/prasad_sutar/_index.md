---
# Display name
title: Mr Prasad Sutar

# Is this the primary user of the site?
superuser: false

# Role/position/tagline
role: "<br> Research Software Engineer"

user_groups: ["Episode 1 Speakers"]

# Organizations/Affiliations to show in About widget

organizations:  
  - name: University of Bristol, UK
  - url: 

# Short bio (displayed in user profile at end of posts)
bio: |

# Interests to show in About widget
interests: 
 - <br> I work as RSE on the project called OpenGHG software dealing with data sharing and analysis hub for greenhouse gases research. Before joining the RSE team, I worked on Systems Development Integration and Modelling team within Rolls-Royce and Software development and automation testing projects at Infosys Ltd. I am Bristol alumnus from MSc Robotics 2022 batch and participated in the research and development of Sparse Training of Deep Predictive Coding Network that involved reducing the dependency on High Computing resources from complex training of models. I am focused on aligning IT practices with sustainability principles and lowering the environmental impacts of the software.


# Education to show in About widget
education:


# Social/Academic Networking
# For available icons, see: https://sourcethemes.com/academic/docs/page-builder/#icons
#   For an email link, use "fas" icon pack, "envelope" icon, and a link in the
#   form "mailto:your-email@example.com" or "/#contact" for contact widget.
social:
- icon: linkedin
  icon_pack: fab
  link: https://www.linkedin.com/in/prasadsutar11/
# - icon: building-user
#   icon_pack: fa
#   link: https://www.ndcn.ox.ac.uk/team/gaurav-bhalerao
# - icon: user
#   icon_pack: fa
#   link: https://orcid.org/0000-0002-3325-3413
- icon: envelope
  icon_pack: fa
  link: mailto:prasadsut11@gmail.com

# Link to a PDF of your resume/CV.
# To use: copy your resume to `static/media/resume.pdf`, enable `ai` icons in `params.toml`, 
# and uncomment the lines below.
# - icon: cv
#   icon_pack: ai
#   link: media/resume.pdf

# Enter email to display Gravatar (if Gravatar enabled in Config)
email: ""

# Highlight the author in author lists? (true/false)
highlight_name: true



---

