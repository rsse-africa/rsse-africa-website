---
# Display name
title: Peter van Heusden

# Is this the primary user of the site?
superuser: false

# Role/position/tagline
role: "Bioinformatician"

user_groups: ["Episode 2 Speakers"]

# Organizations/Affiliations to show in About widget

organizations:  
  - name: South African National Bioinformatics Institute / University of the Western Cape
  - url: 

# Short bio (displayed in user profile at end of posts)
bio: |

# Interests to show in About widget
interests: 
 - <br> I am a bioinformatician with a computer science and computer systems infrastructure background. My focus at SANBI is building tools to enhance genomic surveillance of some of the world's most deadly pathogens (such as M. tuberculosis). I do so by focusing on translating bioinformatics from research to accessible, reproducible workflows applicable to public health needs.


# Education to show in About widget
education:


# Social/Academic Networking
# For available icons, see: https://sourcethemes.com/academic/docs/page-builder/#icons
#   For an email link, use "fas" icon pack, "envelope" icon, and a link in the
#   form "mailto:your-email@example.com" or "/#contact" for contact widget.
social:
- icon: linkedin
  icon_pack: fab
  link: https://www.linkedin.com/in/peter-van-heusden-030b082/

# Link to a PDF of your resume/CV.
# To use: copy your resume to `static/media/resume.pdf`, enable `ai` icons in `params.toml`, 
# and uncomment the lines below.
# - icon: cv
#   icon_pack: ai
#   link: media/resume.pdf

# Enter email to display Gravatar (if Gravatar enabled in Config)
email: ""

# Highlight the author in author lists? (true/false)
highlight_name: true



---

