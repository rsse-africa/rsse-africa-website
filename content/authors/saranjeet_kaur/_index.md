---
# Display name
title: Saranjeet Kaur Bhogal

# Is this the primary user of the site?
superuser: false

# Role/position/tagline
role: "Research Software Engineer"

user_groups: ["Programme Committee"]

# Organizations/Affiliations to show in About widget

organizations:  
  - name: Imperial College London
  - url: https://www.linkedin.com/in/saranjeet-k-48ab769b/

# Short bio (displayed in user profile at end of posts)
bio: |


# Interests to show in About widget
interests: 
  - Saranjeet is a Research Software Engineer with a background in Statistics. She is a strong advocate of Communities of Practice, Open Science, and Open Source. She is the Lead and Co-founder of the RSE Asia Association and was an International Fellow of the 2023 Software Sustainability Institute. In 2022 Saranjeet was awarded the Research Software Engineering Impact Award by the Society of Research Software Engineering.


# Education to show in About widget
education:


# Social/Academic Networking
# For available icons, see: https://sourcethemes.com/academic/docs/page-builder/#icons
#   For an email link, use "fas" icon pack, "envelope" icon, and a link in the
#   form "mailto:your-email@example.com" or "/#contact" for contact widget.
social:
- icon: linkedin
  icon_pack: fab
  link: https://www.linkedin.com/in/saranjeet-k-48ab769b/
- icon: envelope
  icon_pack: fa
  link: "mailto:kaur.saranjeet3@gmail.com"



# Link to a PDF of your resume/CV.
# To use: copy your resume to `static/media/resume.pdf`, enable `ai` icons in `params.toml`, 
# and uncomment the lines below.
# - icon: cv
#   icon_pack: ai
#   link: media/resume.pdf

# Enter email to display Gravatar (if Gravatar enabled in Config)
email: ""

# Highlight the author in author lists? (true/false)
highlight_name: false
---

