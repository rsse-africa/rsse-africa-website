---
widget: blank
headless: true  # This file represents a page section.
title: "Past Episodes" 
subtitle: |
    If you missed it, you can still access recordings and resources!
weight: 30

design:
    columns: "1"
---

<div class="row">
    <h3> <a href="../events-rsse-africa/2024-10-10/">Episode 1: A Conversation with Researchers Who Code</a></h3>         
</div>

10 October 2024 @ 08:30 - 10:00 UTC <a href="https://www.timeanddate.com/worldclock/fixedtime.html?msg=Research+Software+and+Systems+in+Africa+and+Asia&iso=20241010T0830&p1=1440&ah=1&am=30">(find your local time here)</a>

<strong>* Our inaugural meetup coincided with <a href="https://society-rse.org/international-rse-day-october-10th-2024/">International Research Software Engineering Day!</a></strong>

<div class="row">
   <h3> <a href="../events-rsse-africa/2024-11-14/">Episode 2: Enabling Reproducibility through Research Code</a></h3>
</div>

14 November 2024 @ 08:30 - 10:00 UTC <a href="https://www.timeanddate.com/worldclock/fixedtime.html?msg=Reproducibility+and+Research+Software&iso=20241114T0830&p1=1440&ah=1&am=30">(find your local time here)</a>



<div class="row">
    <h3> <a href="../events-rsse-africa/2024-12-12/">Episode 3: Opening Up Research Code</a></h3>
</div>


12 December 2024 @ 08:30 - 10:00 UTC <a href="https://www.timeanddate.com/worldclock/fixedtime.html?iso=20241212T0830&p1=1440&ah=1&am=30">(find your local time here)</a>



<div class="row">
    <h3> <a href="../events-rsse-africa/2025-01-23/">Episode 4: Documentation for Research Code</a></h3>
</div>

23 January 2025 @ 08:30 - 10:00 UTC <a href="https://www.timeanddate.com/worldclock/fixedtime.html?msg=Documentation+for+Research+Code&iso=20250123T0830&p1=1440&ah=1&am=30">(find your local time here)</a>

<div class="row">
    <h3> <a href="../events-rsse-africa/2025-02-20/">Episode 5: Testing Research Code</a></h3>
</div>

20 February 2025 @ 08:30 - 10:00 UTC <a href="https://www.timeanddate.com/worldclock/fixedtime.html?msg=Testing+Research+Code&iso=20250220T0830&p1=1440&ah=1&am=30">(find your local time here)</a>

